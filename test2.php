<!DOCTYPE html>

<html  lang="en-US"><!--<![endif]-->
<head>
<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="engine1/style.css" />
<script type="text/javascript" src="engine1/jquery.js"></script>

<link rel="stylesheet" id="xenia-fontawesome-css" href="services/font-awesome.min.css" type="text/css" media="all">

<link href="http://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="font" href="fonts/fontawesome-webfont.ttf">
<link rel="stylesheet" type="font" href="fonts/fontawesome-webfont.woff">
<link rel="stylesheet" id="xenia-bxslider-css" href="services/jquery.bxslider.css" type="text/css" media="all">
<link rel="stylesheet" id="xenia-testimonialrotator-css" href="services/testimonialrotator.css" type="text/css" media="all">
<link rel="stylesheet" id="xenia-responsive-css" href="services/responsive.css" type="text/css" media="all">
<link rel="stylesheet" id="xenia-magnific-css" href="services/magnific.css" type="text/css" media="all">
<link rel="stylesheet" id="js_composer_front-css" href="services/js_composer.css" type="text/css" media="all">
<!--link rel="stylesheet" id="js_composer_custom_css-css" href="services/custom.css" type="text/css" media="screen"-->
<link rel="stylesheet" id="asalah_bootstrap_css-css" href="bostan/framework/bootstrap/css/bootstrap.min.css" type="text/css" media="all">

<link rel="stylesheet" id="asalah_fontello_css-css" href="bostan/framework/fontello/css/fontello.css" type="text/css" media="all">


<link rel="stylesheet" id="asalah_isotope_css-css" href="bostan/js/isotope/style.css" type="text/css" media="all">
<link rel="stylesheet" href="bostan/js/assets/owl.carousel.min.css">
<link rel="stylesheet" href="bostan/js/assets/owl.theme.default.min.css">

<script type="text/javascript">
</script>
<script
  src="https://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous"></script>
<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script-->
<script type="text/javascript" src="services/jquery-migrate.min.js.download"></script>
<script type="text/javascript" src="services/jquery.themepunch.tools.min.js.download"></script>
<script type="text/javascript" src="services/jquery.themepunch.revolution.min.js.download"></script>
<script src="bostan/js/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.8/handlebars.min.js"></script>

<script type="text/javascript" src="services/modernizr.custom.js.download"></script>
<!--script type="text/javascript" src="./Project one _ Just another WordPress site_files/jquery.js.download"></script-->
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://localhost:8888/wordpress/xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://localhost:8888/wordpress/wp-includes/wlwmanifest.xml"> 
<meta name="generator" content="WordPress 4.1.15">
<link rel="canonical" href="http://localhost:8888/wordpress/?page_id=254">
<link rel="shortlink" href="http://localhost:8888/wordpress/?p=254">
<!-- script type="text/javascript" src="bostan/js/carousel/jquery.carouFredSel-6.2.0-packed.js?ver=4.1.15"></script -->
<script type="text/javascript" src="bostan/js/carousel/jquery.carouFredSel-6.2.0-packed.js?ver=4.1.15"></script>
<script type="text/javascript" src="dist/js/countUp.js"></script>
<script type="text/javascript" src="dist/js/countUp-jquery.js"></script>	
<link rel="stylesheet" id="asalah_main_style-css" href="bostan/style_003.css" type="text/css" media="all">
<link rel="stylesheet" href="dist/css/local_style.css" type="text/css" media="all">

<link rel="stylesheet" id="aqpb" href="bostan/framework/aqua/assets/css/aqpb.css" type="text/css" media="all">
<link rel="stylesheet" id="aqpb" href="bostan/framework/aqua/assets/css/aqpb_blocks.css" type="text/css" media="all">
<link rel="stylesheet" id="aqpb" href="bostan/framework/aqua/assets/css/aqpb-view.css" type="text/css" media="all">
<style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1409410673954{margin-top: 35px !important;}.vc_custom_1409411678326{margin-top: 100px !important;}.vc_custom_1409411651091{margin-top: 100px !important;}.vc_custom_1409410418238{margin-top: 75px !important;}.vc_custom_1409410777701{margin-top: 10px !important;}.vc_custom_1409419254152{margin-top: 75px !important;}</style>    </head>




</head>

    <body class="home page page-id-23648 page-template page-template-page-builder page-template-page-builder-php body_width">
		




<style id="rs-plugin-settings-inline-css" type="text/css">



</style>



    
<style>				


.logo {
margin-top:30px;
}
.main_navbar {
margin-top:20px;
}

.page_title_holder h1 {
color:#fff;
}

.page_title_holder a{
color:#fff;
}

.clients_list  li img {
 float:left;
    width: 100%;
}

.portfolio_thumbnail img {
    border-radius: 4px;
    width: 100%;
}

.page_title_holder{
color:#fff;
}



img {
/*	width:auto !important;*/
}

html {
overflow-x: scroll;
overflow-y: scroll;
}

#app{
    width: 70%;
    margin: auto;
}
.block {
    display: flex;
    flex-wrap: wrap;
    margin-bottom: 20px;
}
.owl-carousel {
    display:flex !important;
    flex-wrap: wrap;
    padding-right: 27px;
}
.owl-stage-outer {
    order: 2;
}
.owl-nav {
    order: 1;
    flex-basis: 70px;
    margin-top: -47px;
    margin-bottom: 34px;
    display: flex;
    margin-right: -28px;
    margin-left: auto;
}
.wp-post-image{
    height: 230px;
}
.page_header_title {
    flex-basis: 102%;
    margin-left: -21px;
    border-bottom: 2px solid #26BDEF;
}

</style>          
        <!-- start site header -->

   <?php

$header = file_get_contents('header.html');
echo $header;?>

<div class="container-fluid body_width">
	<div class="row">
        <div class="parallaxprod">


                <div class="parallaxcenterabout">
                    <div class="col-md-6 col-md-offset-5">
                        <p class="moto-text_system_6"> Products </p>
                    </div>
                    
                </div>
            </div>
     </div>        	
</div>	


<br>
 <!-- ********************* !-->
 <div>
    <script id="slide-show" type="text/x-handlebars-template">
        <div class="block">
            <div class="page_header_title" style="color: black"><h3>{{size}}</h3></div>
            <div class="owl-carousel">
                {{#each links}}
                    <img src="dist/image/{{link}}" class="attachment- wp-post-image" alt="logo_1">
                {{/each}}
            </div>
        </div>
    </script>
 	<div id="app">
        
     </div>
 </div>


    
   <script type="text/javascript" language="javascript">
        function gen_slide(label,size,range){
            var a = [];
            for (var i = 1; i <= range; i++){
                a.push({link: size+'/'+i+'.jpg'});
            }
            return {size: label, links: a};
        }
        $(document).ready(function(){
            var source   = $("#slide-show").html();
            var template = Handlebars.compile(source);
            $("#app").append(template(gen_slide('8 x 8', '8x8', 6)));
            $("#app").append(template(gen_slide('4 x 4', '4x4', 7)));
            $("#app").append(template(gen_slide('KPS', 'kps', 7)));
            $("#app").append(template(gen_slide('DM', 'DM', 14)));
            $("#app").append(template(gen_slide('Single', 'single', 14)));
            $("#app").append(template(gen_slide('Uniform', 'uniform', 25)));
            $(".owl-carousel").owlCarousel({
                nav: true,
                margin: 30,
                navText: ['<span class="cars_arrow_control" style="display: block;"> <i class="icon-angle-left"></i></span>',
                          '<span class="cars_arrow_control" style="display: block;"> <i class="icon-angle-right"></i></span>']
            });
        });
                    
    </script>
</br>
<?php 
$footer = file_get_contents('footer.html');
echo $footer;
   ?>     