	<div class="new_section row-fluid content_boxes">
		<div class="single_comments span12">
        <h3 class="page-header"><?php _e('Comments','asalah'); ?></h3>
            <div class="single_comments_box row-fluid">
			<?php if ( post_password_required() ) : ?>
                <p class="nopassword"><?php _e( 'This post is password protected. Enter the password to view any comments.', 'asalah' ); ?></p>
    </div>
    </div>
	</div><!-- #comments -->
	<?php
			/* Stop the rest of comments.php from being processed,
			 * but don't kill the script entirely -- we still have
			 * to fully load the template.
			 */
			return;
		endif;
	?>

	<?php // You can start editing here -- including this comment! ?>

	<?php if ( have_comments() ) : ?>
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-above">
			<h1 class="assistive-text"><?php _e( 'Comment navigation', 'asalah' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'asalah' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'asalah' ) ); ?></div>
		</nav>
		<?php endif; // check for comment navigation ?>

		<ul class="media-list comments_list span12">
			<?php
				wp_list_comments( array( 'callback' => 'asalah_comment' ) );
			?>
		</ul>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-below">
			<h1 class="assistive-text"><?php _e( 'Comment navigation', 'asalah' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'asalah' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'asalah' ) ); ?></div>
		</nav>
		<?php endif; // check for comment navigation ?>

	<?php
		/* If there are no comments and comments are closed, let's leave a little note, shall we?
		 * But we don't want the note on pages or post types that do not support comments.
		 */
		elseif ( ! comments_open() && ! is_page() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="nocomments"><?php _e( 'Comments are closed.', 'asalah' ); ?></p>
	<?php endif; ?>
    <div class="row-fluid">
    <div class="comment_form span12">
        <h4><?php _e('Your Turn To Talk', 'asalah'); ?></h4>

        <?php
        $args = array(
        'id_form' => 'commentform',
        'id_submit' => 'submit',
        'title_reply' => '',
        'title_reply_to' => __( 'Leave a Reply to %s', 'asalah' ),
        'cancel_reply_link' => __( 'Cancel Reply', 'asalah' ),
        'label_submit' => __( 'Post Comment', 'asalah' ),
        'comment_field' => '<div class="row-fluid"><textarea id="comment" name="comment" aria-required="true" class="span12" rows="3"></textarea></div>',
        'must_log_in' => '<p class="must-log-in">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
        'logged_in_as' => '<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
        'comment_notes_before' => '<p class="comment-notes">' . __( 'Your email address will not be published.', 'asalah' ) . ( $req ? $required_text : '' ) . '</p>',
        'comment_notes_after' => '',
        'fields' => apply_filters( 'comment_form_default_fields', array(
            'author' => '<div class="row-fluid"><input id="author" name="author" class="span6" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" placeholder="Name" value="' . esc_attr( $commenter['comment_author'] ) . '" ' . $aria_req . '></div>',
            'email' => '<div class="row-fluid"><input id="email" name="email" class="span6" type="text" placeholder="Email" ' . $aria_req . '></div>',
            'url' => '<div class="row-fluid"><input id="url" name="url" class="span6" type="text" placeholder="Website" value="' . esc_attr( $commenter['comment_author_url'] ) . '"></div>' ) ) );
			?>
        <?php comment_form($args); ?>
    </div>
    </div>
</div>
</div>
</div><!-- #comments -->
