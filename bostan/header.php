<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?> xmlns:fb="http://ogp.me/ns/fb#">
    <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
        <title><?php
            global $page, $paged;
            wp_title('|', true, 'right');
            bloginfo('name');
            $site_description = get_bloginfo('description', 'display');
            if ($site_description && ( is_home() || is_front_page() ))
                echo " | $site_description";
            if ($paged >= 2 || $page >= 2)
                echo ' | ' . sprintf(__('Page %s', 'asalah'), max($paged, $page));
            ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />



        <!-- favicon -->
        <?php global $asalah_data; ?>
        <?php if ($asalah_data['asalah_fav_url']): ?>
            <link rel="shortcut icon" href="<?php echo $asalah_data['asalah_fav_url']; ?>" title="Favicon" />
        <?php endif; ?>

        <?php if ($asalah_data['asalah_apple_57']): ?>
            <link rel=”apple-touch-icon” href=”<?php echo $asalah_data['asalah_apple_57']; ?>” />
        <?php endif; ?>

        <?php if ($asalah_data['asalah_apple_72']): ?>
            <link rel=”apple-touch-icon” sizes=”72×72″ href=”<?php echo $asalah_data['asalah_apple_114']; ?>” />
        <?php endif; ?>

        <?php if ($asalah_data['asalah_apple_114']): ?>
            <link rel=”apple-touch-icon” sizes=”114×114″ href=”<?php echo $asalah_data['asalah_apple_114']; ?>” />
        <?php endif; ?>

        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <?php
        if (is_singular() && get_option('thread_comments'))
            wp_enqueue_script('comment-reply');
        ?>


        <!-- Your custom head codes will go here -->
        <?php if ($asalah_data['asalah_header_code']): ?>
            <?php echo $asalah_data['asalah_header_code']; ?>
        <?php endif; ?>
        <!-- custom head codes-->


        <?php wp_head(); ?>
    </head>

    <body <?php body_class('body_width'); ?>>
        <?php if ($asalah_data['asalah_use_sdk']): ?>
            <!-- Load facebook SDK -->
            <div id="fb-root"></div>
            <script>
                window.fbAsyncInit = function() {
                FB.init({
    <?php if ($asalah_data['asalah_fb_id']): ?>
                    appId      : '<?php echo $asalah_data['asalah_fb_id']; ?>', // App ID
    <?php endif; ?>
                status     : true, // check login status
                        cookie     : true, // enable cookies to allow the server to access the session
                        xfbml      : true  // parse XFBML
                });
                };
                        // Load the SDK Asynchronously
                                (function(d){
                                var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return; }
                                js = d.createElement('script'); js.id = id; js.async = true;
                                        js.src = "//connect.facebook.net/en_US/all.js";
                                        d.getElementsByTagName('head')[0].appendChild(js);
                                }(document));
            </script>
            <!-- End Load facebook SDK -->
<?php endif; ?>

        <!-- start site header -->
        <header class="header_container body_width">
            <!-- start top header -->
            <div class="container-fluid top_header">
                <div class="container">
                    <div class="row-fluid">

                        <?php if($asalah_data['asalah_tw_url'] || $asalah_data['asalah_fb_url'] || $asalah_data['asalah_gp_url'] || $asalah_data['asalah_linked_url'] || $asalah_data['asalah_youtube_url'] || $asalah_data['asalah_vimeo_url'] || $asalah_data['asalah_vk_url'] || $asalah_data['asalah_instagram_url'] || $asalah_data['asalah_pin_url'] || $asalah_data['asalah_500px_url'] || $asalah_data['asalah_github_url'] || $asalah_data['asalah_flickr_url'] || !$asalah_data['asalah_disable_rss']): ?>
                        <!-- start header tools span -->
                        <div class="top_header_tools_holder pull-right">
                            <div class="header_items_line ">
                                <div class="social_icons pull-right">
                                    <ul class="social_icons_list clearfix">
<?php asalah_social_icons(); ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- end header tools span -->
                        <?php endif; ?>

                        <!-- start contact info span -->
<?php if ($asalah_data['asalah_header_phone'] || $asalah_data['asalah_header_mail']) : ?> 
                            <div class="contact_info_holder">
                                <div class="contact_info_line">
    <?php if ($asalah_data['asalah_header_mail']) : ?> 
                                        <i class="icon-mail contact_info_icon"></i> <span class="mail_address contact_info_item"><?php echo $asalah_data['asalah_header_mail']; ?></span>
                                    <?php endif; ?>

                                    <?php if ($asalah_data['asalah_header_phone']) : ?> 
                                        <i class="icon-phone-outline contact_info_icon"></i> <span class="phone_number contact_info_item"><?php echo $asalah_data['asalah_header_phone']; ?></span> 
                                    <?php endif; ?>


                                </div>

                            </div>
<?php endif; ?>
                        <!-- end contact info span -->


                    </div>
                </div>
            </div>
            <!-- end top header -->

            <!-- start below header -->
            <div id="below_header" class="container-fluid body_width below_header <?php if ($asalah_data['asalah_sticky_header']) { ?>headerissticky<?php } ?>">
                <div class="container">
                    <div class="row"><div id="below_header_span" class="span12">
                            <div class="row-fluid">
                                <div class="logo">
<?php if ($asalah_data['asalah_logo_url']): ?>
                                        <a class="default_logo" href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><h1><img width="<?php echo $asalah_data['asalah_logo_url_w']; ?>" height="<?php echo $asalah_data['asalah_logo_url_h']; ?>"  src="<?php echo $asalah_data['asalah_logo_url']; ?>" alt="<?php bloginfo('name'); ?>"><strong class="hidden"><?php bloginfo('name'); ?></strong></h1></a>
                                        <?php if ($asalah_data['asalah_logo_url_retina']) { ?>
                                            <a class="retina_logo" href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><h1><img width="<?php echo $asalah_data['asalah_logo_url_w']; ?>" height="<?php echo $asalah_data['asalah_logo_url_h']; ?>" src="<?php echo $asalah_data['asalah_logo_url_retina']; ?>" alt="<?php bloginfo('name'); ?>"><strong class="hidden"><?php bloginfo('name'); ?></strong></h1></a>
                                        <?php } ?>
                                    <?php else: ?>

                                        <a href="<?php echo home_url(); ?>"><h1><?php echo get_bloginfo('name'); ?></h1></a>
<?php endif; ?>
                                </div>
                                <div id="gototop" title="<?php _e('Scroll To Top', 'asalah'); ?>" class="gototop pull-right">
                                    <i class="icon-up-open"></i>
                                </div>
                                <nav class="span visible-desktop span navbar main_navbar pull-right">
<?php
wp_nav_menu(array(
    'container' => 'div',
    'container_class' => 'main_nav',
    'theme_location' => 'primarymenu',
    'menu_class' => 'nav',
    'fallback_cb' => '',
    'walker' => new Bootstrap_Walker(),
));
?>
                                </nav>
                                <div class="mobile primary_menu visible-phone visible-tablet pull-right">
                                    <div id="mobile_menu" class="menu row-fluid">
                                        <?php echo asalah_mobile_menu(array('menu_name' => 'primarymenu', 'id' => 'primary_menu_mobile', 'class' => 'top_menu_mobile span12 mobile_menu_select')) ?>
                                    </div>
                                </div>
                            </div></div></div>
                </div>
            </div>
            <!-- end below header -->

            <div class="header_shadow_separator"></div>
        </header>
        <!-- end site header -->

        <div class="body_width site_middle_content">