<?php get_header(); ?>


<!-- post title holder -->
<?php if (get_post_meta($post->ID, 'asalah_title_holder', true) != 'hide'): ?>
    <?php if (get_post_meta($post->ID, 'asalah_custom_title_bg', true)): ?>
    <style>
    .page_title_holder {
        background-image: url('<?php echo get_post_meta($post->ID, 'asalah_custom_title_bg', true);  ?>');
        background-repeat: no-repeat;
    }    
    </style>
    <?php endif; ?>
    <div class="page_title_holder container-fluid">
            <div class="container">
                    <div class="page_info">
                            <h1><?php the_title(); ?></h1>
                            <?php asalah_breadcrumbs(); ?>
                    </div>
                    <div class="page_nav clearfix">
                    <?php
                                            $next_post = get_next_post();
                                            $prev_post = get_previous_post();
                                            ?>

                        <?php if (!empty( $prev_post )): ?>
                            <a title="<?php _e("Next Project", 'asalah'); ?>" href="<?php echo get_permalink( $prev_post->ID ); ?>" id="right_nav_arrow" class="cars_nav_control right_nav_arrow"><i class="icon-angle-right"></i></a>
                        <?php endif; ?>

                        <?php if ($asalah_data['asalah_portfolio_url']): ?>
                            <a href="<?php echo $asalah_data['asalah_portfolio_url']; ?>" id="main_portfolio_arrow" class="cars_portfolio_control"><i class="icon-th-large"></i></a>
                        <?php endif; ?>

                        <?php if (!empty( $next_post )): ?>
                            <a title="<?php _e("Previous Project", 'asalah'); ?>" href="<?php echo get_permalink( $next_post->ID ); ?>" id="left_nav_arrow" class="cars_nav_control left_nav_arrow"><i class="icon-angle-left"></i></a>
                        <?php endif; ?>
                    </div>
            </div>
    </div>
<?php endif; ?>
<!-- end post title holder -->

<section class="main_content">
	<div class="container single_blog blog_posts new_section">
		<div class="row-fluid">
        	<?php if (get_post_meta($post->ID, 'asalah_post_layout', true) != 'full'): ?>
			<div class="span9 blog_main_content <?php if (get_post_meta($post->ID, 'asalah_post_layout', true) == 'left') { echo " pull-right";} ?>">
				<?php if ( $wp_query ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
				<?php $get_meta = get_post_custom($post->ID); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class('blog_post row-fluid'); ?>>
                    
                    	<?php if (get_post_meta($post->ID, 'asalah_post_meta_info', true) != 'hide' ): ?>
						<div class="blog_info clearfix">
                            <div class="blog_box_item post_type_box_item">
                                <?php asalah_blog_icon(); ?>
                            </div>
							<div class="blog_heading">
								<div class="blog_title">
									<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
								</div>
                                <div class="blog_info_box clearfix">
                                    
                                    
                                    <div class="blog_box_item"><span class="blog_date meta_item"><i class="icon-calendar meta_icon"></i> <?php echo get_the_date(); ?></span></div>
                                    <div class="blog_box_item"><span class="blog_comments meta_item"><i class="icon-comment meta_icon"></i> <?php comments_number(); ?></span></div>
                                    
                                    <?php if (get_the_category_list()): ?>
                                    <div class="blog_box_item"><span class="blog_category meta_item"><i class="icon-folder-open meta_icon"></i> <?php echo get_the_category_list(', ' ); ?></span></div>
                                    <?php endif; ?>
                                    
                                </div>
							</div>
                            
						</div>
                        <?php endif; ?>
                        <?php if (get_post_meta($post->ID, 'asalah_post_type', true) != ''): ?>
                        <div class="blog_banner clearfix">
							<?php asalah_banner() ?>
						</div>
                        <?php endif; ?>
						<div class="blog_description">
							<?php the_content(); wp_reset_query(); ?>
							
							<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'twentytwelve' ), 'after' => '</div>' ) ); ?>
                        	<div class="blog_post_tags clearfix"><?php the_tags("", "", ""); ?></div>
							<?php 
							if (get_post_meta($post->ID, 'asalah_post_share_box', true) == 'show' ):
								asalah_post_share(); 
							endif;
							?>
						</div>
						
						<!-- start post author box -->
						<?php if ($get_meta['asalah_post_author_box'][0] != 'hide' ): ?>
                        <div class="new_section">
						<div class="row-fluid content_boxes clearfix">
							<div class="about_author span12">
                            	<h3 class="page-header"><?php _e('About Author','asalah'); ?></h3>
								<div class="author_info_box clearfix">
									<div class="author_image"><?php echo get_avatar( get_the_author_meta('ID'), 100); ?></div>
									<div class="author_info">
											<div class=" author_name"><h4><?php the_author(); ?></h4></div>
											<div class=" author_desc">
											<p><?php echo get_the_author_meta('description'); ?></p>
											</div>
											<div class=" author_social_profiles">
												<ul>
												
													<?php if ( get_the_author_meta('url') ): ?>
													<li><a href="<?php the_author_meta('url'); ?>
		" class="profile_social_icon icon-globe"></a></li>
													<?php endif; ?>
													<?php if ( get_the_author_meta('twitter') ): ?>
													<li><a href="<?php the_author_meta('twitter'); ?>
		" class="profile_social_icon icon-twitter"></a></li>
													<?php endif; ?>
													<?php if ( get_the_author_meta('facebook') ): ?>
													<li><a href="<?php the_author_meta('facebook'); ?>
		" class="profile_social_icon icon-facebook"></a></li>
													<?php endif; ?>
													<?php if ( get_the_author_meta('plus') ): ?>
													<li><a href="<?php the_author_meta('plus'); ?>
		" class="profile_social_icon icon-google-plus"></a></li>
													<?php endif; ?>
													<?php if ( get_the_author_meta('linkedin') ): ?>
													<li><a href="<?php the_author_meta('linkedin'); ?>
		" class="profile_social_icon icon-linkedin"></a></li>
													<?php endif; ?>

												</ul>
											</div>
									</div>
								</div>
							</div>
						</div>
                        </div>
						<?php endif; ?>
						<!-- end post author box -->
						
						<?php if (comments_open()) { ?>
						<!-- start post comments -->
						<?php comments_template( '', true ); ?>
						<!-- end post comments -->
						<?php } ?>
						
					</article>
				<?php endwhile; ?>
				<?php endif; ?>
			</div>
			<aside class="span3 side_content <?php if (get_post_meta($post->ID, 'asalah_post_layout', true) == 'left') { echo " pull-left";} ?>"> 
				<h3 class="hidden"><?php _e('Post Sidebar','asalah'); ?></h3>
				<?php
					$asalah_have_custom_sidebar = get_post_meta($post->ID, 'asalah_custom_sidebar', true);
			
					if (!isset($asalah_have_custom_sidebar) || $asalah_have_custom_sidebar == '' || $asalah_have_custom_sidebar == 'none') {
						get_sidebar( 'single' );
					}else{
						
						$custom_sidebar_id = get_post_meta($post->ID, 'asalah_custom_sidebar', true);
						if ( is_active_sidebar( $custom_sidebar_id ) ) :
							dynamic_sidebar( $custom_sidebar_id );
						endif;
					}
				?>
			</aside>
            <?php else: ?> <!-- if full width -->
            <div class="span12">
				<?php if ( $wp_query ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
				<?php $get_meta = get_post_custom($post->ID); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class('blog_post row-fluid'); ?>>
						<?php if (get_post_meta($post->ID, 'asalah_post_meta_info', true) != 'hide' ): ?>
                        <div class="blog_info_box clearfix">
                        	<div class="blog_box_item post_type_box_item">
                            	<?php asalah_blog_icon(); ?>
                            </div>
                            <div class="blog_box_item"><span class="blog_date meta_item"><i class="icon-calendar meta_icon"></i> <?php echo get_the_date(); ?></span></div>
                            <div class="blog_box_item"><span class="blog_comments meta_item"><i class="icon-comment meta_icon"></i> <?php comments_number(); ?></span></div>
                            
							<?php if (get_the_category_list()): ?>
                            <div class="blog_box_item"><span class="blog_category meta_item"><i class="icon-folder-open meta_icon"></i> <?php echo get_the_category_list(', ' ); ?></span></div>
                            <?php endif; ?>
                            
                        </div>
                        <?php endif; ?>
                        <?php if (get_post_meta($post->ID, 'asalah_post_type', true) != ''): ?>
                        <div class="blog_banner clearfix">
							<?php asalah_banner() ?>
						</div>
                        <?php endif; ?>
						<div class="blog_description">
							<?php the_content(); wp_reset_query(); ?>
							
							<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'twentytwelve' ), 'after' => '</div>' ) ); ?>
                        
							<?php 
							if (get_post_meta($post->ID, 'asalah_post_share_box', true) == 'show' ):
								asalah_post_share(); 
							endif;
							?>
						</div>
						
						<!-- start post author box -->
						<?php if ($get_meta['asalah_post_author_box'][0] != 'hide' ): ?>
                        <div class="new_section">
						<div class="row-fluid content_boxes">
							<div class="about_author span12">
                            	<h3 class="page-header"><?php _e('About Author','asalah'); ?></h3>
								<div class="author_info_box clearfix">
									<div class="author_image"><?php echo get_avatar( get_the_author_meta('ID'), 100); ?></div>
									<div class="author_info">
											<div class=" author_name"><h4><?php the_author(); ?></h4></div>
											<div class=" author_desc">
											<p><?php echo get_the_author_meta('description'); ?></p>
											</div>
											<div class=" author_social_profiles">
												<ul>
												
													<?php if ( get_the_author_meta('url') ): ?>
													<li><a href="<?php the_author_meta('url'); ?>
		" class="profile_social_icon icon-globe"></a></li>
													<?php endif; ?>
													<?php if ( get_the_author_meta('twitter') ): ?>
													<li><a href="<?php the_author_meta('twitter'); ?>
		" class="profile_social_icon icon-twitter"></a></li>
													<?php endif; ?>
													<?php if ( get_the_author_meta('facebook') ): ?>
													<li><a href="<?php the_author_meta('facebook'); ?>
		" class="profile_social_icon icon-facebook"></a></li>
													<?php endif; ?>
													<?php if ( get_the_author_meta('plus') ): ?>
													<li><a href="<?php the_author_meta('plus'); ?>
		" class="profile_social_icon icon-google-plus"></a></li>
													<?php endif; ?>
													<?php if ( get_the_author_meta('linkedin') ): ?>
													<li><a href="<?php the_author_meta('linkedin'); ?>
		" class="profile_social_icon icon-linkedin"></a></li>
													<?php endif; ?>

												</ul>
											</div>
									</div>
								</div>
							</div>
						</div>
                        </div>
						<?php endif; ?>
						<!-- end post author box -->
						
						<?php if (comments_open()) { ?>
						<!-- start post comments -->
						<?php comments_template( '', true ); ?>
						<!-- end post comments -->
						<?php } ?>
						
					</article>
				<?php endwhile; ?>
				<?php endif; ?>
			</div>
            <?php endif; ?> <!-- end if full width -->
		</div>
	</div>
<?php get_footer(); ?>