<?php
// thumbnails
function magic_substr($haystack, $start, $end) {
	$index_start = strpos($haystack, $start);
	$index_start = ($index_start === false) ? 0 : $index_start + strlen($start);
	if (strpos($haystack, $end) == TRUE) {
		$index_end = strpos($haystack, $end, $index_start); 
		$length = ($index_end === false) ? strlen($end) : $index_end - $index_start;
		return substr($haystack, $index_start, $length);
	}else{
		return substr($haystack, $index_start);
	}
}

function asalah_default_image() {
	global $asalah_data;
	if ($asalah_data['asalah_default_image']) {
		return $asalah_data['asalah_default_image'];
	}else{
		return get_template_directory_uri() . '/img/default.jpg';
	}
}


function asalah_video_prov($vurl) {
	if (strpos($vurl,'youtube') !== false) {
		$prov = "youtube";
	}elseif (strpos($vurl,'youtu') !== false) {
		$prov = "youtu";
	}elseif (strpos($vurl,'vimeo') !== false) {
		$prov = "vimeo";
	}else{
		$prov = "none";
	}
	return $prov;
}

function asalah_video_id($prov, $vurl) {
	if ($prov == 'youtube') {
		$id = magic_substr($vurl, "http://www.youtube.com/watch?v=", "&");
	}elseif ($prov == 'youtu') {
		$id = magic_substr($vurl, "http://www.youtu.be/watch?v=", "&");
	}elseif ($prov == 'vimeo') {
		$id = magic_substr($vurl, "http://vimeo.com/", "?");
	}
	return $id;
}

function asalah_video_iframe($prov, $vid, $height="400") {
	echo '<div class="video_fit_container">';
	if ($prov == 'youtube') {
		?>
		<iframe class="video_iframe" src="http://www.youtube.com/embed/<?php echo $vid; ?>?wmode=transparent&wmode=opaque" frameborder="0" allowfullscreen></iframe>
		<?php
	}elseif ($prov == 'youtu') {
		?>
		<iframe  class="video_iframe" src="http://www.youtube.com/embed/<?php echo $vid; ?>?wmode=transparent&wmode=opaque" frameborder="0" allowfullscreen></iframe>
		<?php
	}elseif ($prov == 'vimeo') {
		?>
		<iframe class="video_iframe" src="http://player.vimeo.com/video/<?php echo $vid; ?>?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
		<?php
	}else{
	
	}
	echo '</div>';
}

function asalah_video_thumb($prov, $vid) {
	if ($prov == 'youtube' || $prov == 'youtu')  {
		$theimage = 'http://img.youtube.com/vi/'.$vid.'/0.jpg';
	}elseif ($prov == 'vimeo') {
		$imgid = $vid;
		$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".$imgid.".php"));
		$theimage = $hash[0]['thumbnail_large'];
	}
	return $theimage;
}

function catch_that_image($id="") {
  if ($id == '') {
		global $post, $posts;
		$post_id = $post->ID;
		$content = $post->post_content;
	}else{
		global $post, $posts;
		$post_id = $id;
		$content_post = get_post($post_id);
		$content = $content_post->post_content;	
	}
  
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
  if (isset($matches [1] [0])) {
  $first_img = $matches [1] [0];
  }

  if(($first_img) == ''){
    $first_img = asalah_default_image();
  }
  return $first_img;
}

function asalah_post_thumb($id = ""){
	if ($id == '') {
		global $post;
		$post_id = $post->ID;
	}else{
		global $post;
		$post_id = $id;	
	}
	
	$type = get_post_meta($post_id, 'asalah_post_type', true);
	if ($type == 'video') {
		$vurl = get_post_meta($post_id, 'asalah_video_url', true);
		$prov = asalah_video_prov($vurl);
		$vid = asalah_video_id($prov, $vurl);
		if ( has_post_thumbnail($post_id) ){
			$image_id = get_post_thumbnail_id($post_id);  
			$image_url = wp_get_attachment_image_src($image_id,'large');  
			$image_url = $image_url[0];
			$theimage = $image_url;
		}else{
			$theimage = asalah_video_thumb($prov, $vid);
		}
	}elseif ( has_post_thumbnail($post_id) ){
		$image_id = get_post_thumbnail_id($post_id);  
		$image_url = wp_get_attachment_image_src($image_id,'large');  
		$image_url = $image_url[0];
		$theimage = $image_url;
	}else{
		$theimage = catch_that_image($post_id);
	}
	return $theimage;
}



function asalah_slideshow() {
	global $post;
	$type = get_post_meta($post->ID, 'asalah_post_type', true);
	if ($type == 'video') {
		$vurl = get_post_meta($post->ID, 'asalah_video_url', true);
		$prov = asalah_video_prov($vurl);
		$vid = asalah_video_id($prov, $vurl);
		
		if (strpos($vurl,'youtube') !== false) {
			echo '<a href="http://www.youtube.com/watch?v='.$vid.'" class="prettyPhoto slideshow_'.$post->ID.'" rel="prettyPhoto"></a>';
		}elseif (strpos($vurl,'youtu') !== false) {
			echo '<a href="http://www.youtube.com/watch?v='.$vid.'" class="prettyPhoto slideshow_'.$post->ID.'" rel="prettyPhoto"></a>';
		}elseif (strpos($vurl,'vimeo') !== false) {
			echo '<a href="http://vimeo.com/'.$vid.'" class="prettyPhoto slideshow_'.$post->ID.'" rel="prettyPhoto"></a>';
		}
		?>	
		<a href="<?php echo asalah_post_thumb(); ?>" class="prettyPhoto slideshow_<?php echo $post->ID; ?>" rel="prettyPhoto[<?php echo $post->ID; ?>]"></a>
		<?php
	}elseif ($type == 'soundcloud') {
		$soundcloudurl = get_post_meta($post->ID, 'asalah_soundcloud_url', true);
		?>
        <header class="content_banner blog_post_banner span12 clearfix">
        <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=<?php echo $soundcloudurl; ?>"></iframe>
        </header>
		<?php
	}elseif ($type == 'featured' ){
		?>
        <a href="<?php echo asalah_post_thumb(); ?>" class="prettyPhoto slideshow_<?php echo $post->ID; ?>" rel="prettyPhoto"></a>
		<?php
	}elseif ($type == 'attached') {
		$attachments = get_children( array('post_parent' => $post->ID, 'post_type' => 'attachment', 'post_mime_type' => 'image') );
		foreach ( $attachments as $attachment_id => $attachment ) {
			$image_attributes =wp_get_attachment_url($attachment_id);
			?>
			<a href="<?php echo $image_attributes; ?>" class="prettyPhoto slideshow_<?php echo $post->ID; ?>" rel="prettyPhoto[<?php echo $post->ID; ?>]"></a>
            <?php
		}
	}
}

function get_asalah_slideshow() {
	global $post;
	$type = get_post_meta($post->ID, 'asalah_post_type', true);
	if ($type == 'video') {
		$vurl = get_post_meta($post->ID, 'asalah_video_url', true);
		$prov = asalah_video_prov($vurl);
		$vid = asalah_video_id($prov, $vurl);
		
		if (strpos($vurl,'youtube') !== false) {
			return '<a href="http://www.youtube.com/watch?v='.$vid.'" class="prettyPhoto slideshow_'.$post->ID.'" rel="prettyPhoto"></a>';
		}elseif (strpos($vurl,'youtu') !== false) {
			return '<a href="http://www.youtube.com/watch?v='.$vid.'" class="prettyPhoto slideshow_'.$post->ID.'" rel="prettyPhoto"></a>';
		}elseif (strpos($vurl,'vimeo') !== false) {
			return '<a href="http://vimeo.com/'.$vid.'" class="prettyPhoto slideshow_'.$post->ID.'" rel="prettyPhoto"></a>';
		}
	}elseif ($type == 'soundcloud') {
		$soundcloudurl = get_post_meta($post->ID, 'asalah_soundcloud_url', true);
		?>
        <header class="content_banner blog_post_banner span12 clearfix">
        <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=<?php echo $soundcloudurl; ?>"></iframe>
        </header>
		<?php
	}elseif ($type == 'featured' ){
        return '<a href="'. asalah_post_thumb().'" class="prettyPhoto slideshow_'. $post->ID.'" rel="prettyPhoto"></a>';
	}elseif ($type == 'attached') {
		$attachments = get_children( array('post_parent' => $post->ID, 'post_type' => 'attachment', 'post_mime_type' => 'image') );
		foreach ( $attachments as $attachment_id => $attachment ) {
			$image_attributes =wp_get_attachment_url($attachment_id);
			return '<a href="'. $image_attributes.'" class="prettyPhoto slideshow_'. $post->ID.'" rel="prettyPhoto['. $post->ID.']"></a>';
		}
	}
}


function asalah_blog_thumb($w="370", $h="240",$size="portfolio", $id = "", $class=""){
	global $asalah_data;
	if ($id == '') {
		global $post;
		$post_id = $post->ID;
	}else{
		global $post;
		$post_id = $id;	
	}
	$type = get_post_meta($post_id, 'asalah_post_type', true);
	if ( has_post_thumbnail($post_id) ){
		if (asalah_post_thumb($post_id) != '') {
		?>
			
            
            <?php the_post_thumbnail($size);  ?>
            
		<?php
		}else{
		?>
        	
            <img src="<?php echo asalah_default_image(); ?>" class="featured_image" alt="<?php the_title(); ?>">
            
		<?php	
		}
	}elseif ($type == 'video') {
		$vurl = get_post_meta($post_id, 'asalah_video_url', true);
		$prov = asalah_video_prov($vurl);
		$vid = asalah_video_id($prov, $vurl);
		?>
        	
            <img src="<?php echo asalah_video_thumb($prov, $vid); ?>" width="<?php echo $w; ?>" height="<?php echo $h; ?>" class="featured_image" alt="<?php the_title(); ?>">
            	
		<?php
	}elseif(asalah_post_thumb($post_id) == '') {
		?>
		
		<img src="<?php echo asalah_default_image(); ?>" class="featured_image" alt="<?php the_title(); ?>">
       
        <?php
	}else{
		?>
		
		<img src="<?php echo asalah_default_image(); ?>" width="<?php echo $w; ?>" height="<?php echo $h; ?>" class="featured_image" alt="<?php the_title(); ?>">
        
		<?php
	}
}

function asalah_get_blog_thumb($w="370", $h="240",$size="portfolio", $id = "", $class=""){
	global $asalah_data;
	if ($id == '') {
		global $post;
		$post_id = $post->ID;
	}else{
		global $post;
		$post_id = $id;	
	}
	$type = get_post_meta($post_id, 'asalah_post_type', true);
	if ( has_post_thumbnail($post_id) ){
		if (asalah_post_thumb($post_id) != '') {
			
			return get_the_post_thumbnail($post_id, $size);
			
		}else{
			
			return '<img src="'.asalah_default_image().'" class="featured_image" alt="'.get_the_title().'">';
			
		}
	}elseif ($type == 'video') {
		$vurl = get_post_meta($post_id, 'asalah_video_url', true);
		$prov = asalah_video_prov($vurl);
		$vid = asalah_video_id($prov, $vurl);
        	
			return '<img src="'.asalah_video_thumb($prov, $vid).'" class="featured_image" alt="'.get_the_title().'">';
			
	}elseif(asalah_post_thumb($post_id) == '') {
			
			return '<img src="'.asalah_default_image().'" class="featured_image" alt="'.get_the_title().'">';
			
		
	}else{
			
			return '<img src="'.asalah_default_image().'" class="featured_image" alt="'.get_the_title().'">';
			
		
		
	}
}

function asalah_blog_icon() {
	global $post;
	$type = get_post_meta($post->ID, 'asalah_post_type', true);
	if ($type == 'video') {
		echo '<i class="icon-play" title="Video"></i>';
	}elseif ($type == 'soundcloud') {
		echo '<i class="icon-music" title="Audio"></i>';
	}elseif ($type == 'quote') {
		echo '<i class="icon-quote-left" title="Quotation"></i>';
	}elseif ($type == 'url') {
		echo '<i class="icon-link" title="Link"></i>';
	}elseif ($type == 'featured' ){
		echo '<i class="icon-camera" title="Image"></i>';
	}elseif ($type == 'attached') {
		echo '<i class="icon-picture" title="Gallery"></i>';
	}else{
		echo '<i class="icon-pencil" title="Text"></i>';
	}
}

function asalah_banner() {
	global $asalah_data;
	global $post;
	$type = get_post_meta($post->ID, 'asalah_post_type', true);
	if ($type == 'video') {
		$vurl = get_post_meta($post->ID, 'asalah_video_url', true);
		$prov = asalah_video_prov($vurl);
		$vid = asalah_video_id($prov, $vurl);
		?>	
		<header class="content_banner blog_post_banner span12 clearfix"><?php asalah_video_iframe($prov, $vid) ?></header>
		<?php
	}elseif ($type == 'soundcloud') {
		$soundcloudurl = get_post_meta($post->ID, 'asalah_soundcloud_url', true);
		?>
        <header class="content_banner blog_post_banner span12 clearfix">
        <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=<?php echo $soundcloudurl; ?>"></iframe>
        </header>
		<?php
	}elseif ($type == 'quote') {
		?>
        <header class="content_banner blog_post_banner span12 clearfix">
        <hgroup class="quotation_banner text_banner single_banner"><h2><?php echo get_post_meta($post->ID, 'asalah_quote_text', true) ?></h2><h3><?php echo "- " . get_post_meta($post->ID, 'asalah_quote_author', true) ?></h3></hgroup>
        </header>
		<?php
	}elseif ($type == 'url') {
		?>
		<a target="_blank" href="<?php echo get_post_meta($post->ID, 'asalah_url_destination', true) ?>">
        <header class="content_banner blog_post_banner span12 clearfix">
		<hgroup class="url_banner text_banner single_banner"><h2><?php echo get_post_meta($post->ID, 'asalah_url_destination', true) ?></h2><h3><?php echo "- " . get_post_meta($post->ID, 'asalah_url_text', true) ?></h3></hgroup>
        </header>
		</a>
		<?php
	}elseif ($type == 'featured' ){
		?>
		<header class="content_banner blog_post_banner span12 clearfix">
		<a href="<?php echo asalah_post_thumb(); ?>" class="prettyPhoto" rel="prettyPhoto">
			
            <img src="<?php echo asalah_post_thumb(); ?>" class="featured_image">
            
        </a>
		</header>
		<?php
	}elseif ($type == 'attached') {
		echo '<header class="content_banner blog_post_banner span12 clearfix">';
		$attachments = get_children( array('post_parent' => $post->ID, 'post_type' => 'attachment', 'post_mime_type' => 'image') );
		echo '<div class="galleryslider clearfix"><ul class="slides gallerylist clearfix">';
		foreach ( $attachments as $attachment_id => $attachment ) {
			$image_attributes =wp_get_attachment_url($attachment_id);
			?>
            <?php if (get_post_type() == 'project'): ?>
			<li><a href="<?php echo $image_attributes; ?>" class="prettyPhoto" rel="prettyPhoto[<?php echo $post->ID; ?>]">
            	
                <img src="<?php echo $image_attributes; ?>" class="featured_image">
                
            </a></li>
            <?php elseif(get_post_type() == 'post'): ?>
            <li><a href="<?php echo $image_attributes; ?>" class="prettyPhoto" rel="prettyPhoto[<?php echo $post->ID; ?>]">
            
            <img src="<?php echo $image_attributes; ?>" class="featured_image">
            
            </a></li>
            <?php endif; ?>
            
            <?php
		} 
		echo '</ul></div>';
		echo '</header>';
	}
}

?>