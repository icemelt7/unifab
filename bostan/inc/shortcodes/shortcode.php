<?php
define ( 'JS_PATH' , get_template_directory_uri().'/inc/shortcodes/customcodes.js');


add_action('admin_head','html_quicktags');
function html_quicktags() {

	$output = "<script type='text/javascript'>\n
	/* <![CDATA[ */ \n";
	wp_print_scripts( 'quicktags' );

	$buttons = array();
	
	/*$buttons[] = array(
		'name' => 'raw',
		'options' => array(
			'display_name' => 'raw',
			'open_tag' => '\n[raw]',
			'close_tag' => '[/raw]\n',
			'key' => ''
	));*/


	$buttons[] = array(
		'name' => 'ads1',
		'options' => array(
			'display_name' => 'ADS1',
			'open_tag' => '\n[ads1]',
			'close_tag' => '',
			'key' => ''
	));
	
	$buttons[] = array(
		'name' => 'ads2',
		'options' => array(
			'display_name' => 'ADS2',
			'open_tag' => '\n[ads2]',
			'close_tag' => '',
			'key' => ''
	));

	$buttons[] = array(
		'name' => 'is_logged_in',
		'options' => array(
			'display_name' => 'is logged in',
			'open_tag' => '\n[is_logged_in]',
			'close_tag' => '[/is_logged_in]\n',
			'key' => ''
	));
	
	$buttons[] = array(
		'name' => 'is_guest',
		'options' => array(
			'display_name' => 'is guest',
			'open_tag' => '\n[is_guest]',
			'close_tag' => '[/is_guest]\n',
			'key' => ''
	));

		
	$buttons[] = array(
		'name' => 'one_third',
		'options' => array(
			'display_name' => 'one third',
			'open_tag' => '\n[one_third]',
			'close_tag' => '[/one_third]\n',
			'key' => ''
	));
		
	$buttons[] = array(
		'name' => 'one_third_last',
		'options' => array(
			'display_name' => 'one third last',
			'open_tag' => '\n[one_third_last]',
			'close_tag' => '[/one_third_last]\n',
			'key' => ''
	));
			
	$buttons[] = array(
		'name' => 'two_third',
		'options' => array(
			'display_name' => 'two third',
			'open_tag' => '\n[two_third]',
			'close_tag' => '[/two_third]\n',
			'key' => ''
	));	
	
	$buttons[] = array(
		'name' => 'two_third_last',
		'options' => array(
			'display_name' => 'two third last',
			'open_tag' => '\n[two_third_last]',
			'close_tag' => '[/two_third_last]\n',
			'key' => ''
	));	
	
	$buttons[] = array(
		'name' => 'one_half',
		'options' => array(
			'display_name' => 'one half',
			'open_tag' => '\n[one_half]',
			'close_tag' => '[/one_half]\n',
			'key' => ''
	));	
	
	$buttons[] = array(
		'name' => 'one_half_last',
		'options' => array(
			'display_name' => 'one half last',
			'open_tag' => '\n[one_half_last]',
			'close_tag' => '[/one_half_last]\n',
			'key' => ''
	));	
	
	$buttons[] = array(
		'name' => 'one_fourth',
		'options' => array(
			'display_name' => 'one fourth',
			'open_tag' => '\n[one_fourth]',
			'close_tag' => '[/one_fourth]\n',
			'key' => ''
	));	
	
	$buttons[] = array(
		'name' => 'one_fourth_last',
		'options' => array(
			'display_name' => 'one fourth last',
			'open_tag' => '\n[one_fourth_last]',
			'close_tag' => '[/one_fourth_last]\n',
			'key' => ''
	));
	
	$buttons[] = array(
		'name' => 'three_fourth',
		'options' => array(
			'display_name' => 'three fourth',
			'open_tag' => '\n[three_fourth]',
			'close_tag' => '[/three_fourth]\n',
			'key' => ''
	));
	
	$buttons[] = array(
		'name' => 'three_fourth_last',
		'options' => array(
			'display_name' => 'three fourth last',
			'open_tag' => '\n[three_fourth_last]',
			'close_tag' => '[/three_fourth_last]\n',
			'key' => ''
	));
	
	$buttons[] = array(
		'name' => 'one_fifth',
		'options' => array(
			'display_name' => 'one fifth',
			'open_tag' => '\n[one_fifth]',
			'close_tag' => '[/one_fifth]\n',
			'key' => ''
	));
	
	$buttons[] = array(
		'name' => 'one_fifth_last',
		'options' => array(
			'display_name' => 'one fifth last',
			'open_tag' => '\n[one_fifth_last]',
			'close_tag' => '[/one_fifth_last]\n',
			'key' => ''
	));
	
	$buttons[] = array(
		'name' => 'two_fifth',
		'options' => array(
			'display_name' => 'two fifth',
			'open_tag' => '\n[two_fifth]',
			'close_tag' => '[/two_fifth]\n',
			'key' => ''
	));
	
	$buttons[] = array(
		'name' => 'two_fifth_last',
		'options' => array(
			'display_name' => 'two fifth last',
			'open_tag' => '\n[two_fifth_last]',
			'close_tag' => '[/two_fifth_last]\n',
			'key' => ''
	));
	
	$buttons[] = array(
		'name' => 'three_fifth',
		'options' => array(
			'display_name' => 'three fifth',
			'open_tag' => '\n[three_fifth]',
			'close_tag' => '[/three_fifth]\n',
			'key' => ''
	));
		
	$buttons[] = array(
		'name' => 'three_fifth_last',
		'options' => array(
			'display_name' => 'three fifth last',
			'open_tag' => '\n[three_fifth_last]',
			'close_tag' => '[/three_fifth_last]\n',
			'key' => ''
	));
		
	$buttons[] = array(
		'name' => 'four_fifth',
		'options' => array(
			'display_name' => 'four fifth',
			'open_tag' => '\n[four_fifth]',
			'close_tag' => '[/four_fifth]\n',
			'key' => ''
	));
		
	$buttons[] = array(
		'name' => 'four_fifth_last',
		'options' => array(
			'display_name' => 'four fifth last',
			'open_tag' => '\n[four_fifth_last]',
			'close_tag' => '[/four_fifth_last]\n',
			'key' => ''
	));
		
	$buttons[] = array(
		'name' => 'one_sixth',
		'options' => array(
			'display_name' => 'one sixth',
			'open_tag' => '\n[one_sixth]',
			'close_tag' => '[/one_sixth]\n',
			'key' => ''
	));
		
	$buttons[] = array(
		'name' => 'one_sixth_last',
		'options' => array(
			'display_name' => 'one sixth last',
			'open_tag' => '\n[one_sixth_last]',
			'close_tag' => '[/one_sixth_last]\n',
			'key' => ''
	));
		
	$buttons[] = array(
		'name' => 'five_sixth',
		'options' => array(
			'display_name' => 'five sixth',
			'open_tag' => '\n[five_sixth]',
			'close_tag' => '[/five_sixth]\n',
			'key' => ''
	));
		
	$buttons[] = array(
		'name' => 'five_sixth_last',
		'options' => array(
			'display_name' => 'five sixth last',
			'open_tag' => '\n[five_sixth_last]',
			'close_tag' => '[/five_sixth_last]\n',
			'key' => ''
	));
		
			
	for ($i=0; $i <= (count($buttons)-1); $i++) {
		$output .= "edButtons[edButtons.length] = new edButton('ed_{$buttons[$i]['name']}'
			,'{$buttons[$i]['options']['display_name']}'
			,'{$buttons[$i]['options']['open_tag']}'
			,'{$buttons[$i]['options']['close_tag']}'
			,'{$buttons[$i]['options']['key']}'
		); \n";
	}
	
	$output .= "\n /* ]]> */ \n
	</script>";
	echo $output;
}



function asalah_custom_addbuttons() {
	if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
		return;

	if ( get_user_option('rich_editing') == 'true') {
		add_filter("mce_external_plugins", "add_tcustom_tinymce_plugin");
		add_filter('mce_buttons_3', 'register_tcustom_button');
	}
}
function register_tcustom_button($buttons) {
	array_push(
		$buttons,
		"AddBox",
		"AddAlert",
		"AddButtons",		"Toggle",
		"|",
		"AddMap",
		"|",
		"highlight",
		"dropcap",
		"|",
		"checklist",
		"starlist",
		"|",
		"Video",
		"|",
		"Tooltip",
		"ShareButtons",
		"divider"	); 
	return $buttons;
} 
function add_tcustom_tinymce_plugin($plugin_array) {
	$plugin_array['asalahShortCodes'] = JS_PATH;
	return $plugin_array;
}
add_action('init', 'asalah_custom_addbuttons');


## Team -------------------------------------------------- #
function asalah_shortcode_team( $atts, $content = null ) {
    @extract($atts);
	//title
	//number
	$title = ($title) ? ' '.$title : __('Team', 'asalah');
	$number = ($number) ? ' '.$number : '4';
	$max = ($max) ? ' '.$max : '4';
	$block_id = "team_car_".random_id(25);
	$url ="";
	$number;
	$desc = "";
	$cycle ="";
	$wp_query = new WP_Query(array('post_type' => 'team', 'posts_per_page' => $number));
	
	
	
	$out = '<div class="team_carousel team_car" id="'. $block_id.'">';
		$out .= '<h3 class="page-header"><span class="page_header_title">'.$title.'</span><span class="right_car_arrow3 cars_arrow_control right_car_arrow hidden" style="display: none;"><i class="icon-angle-right"></i></span><span class="left_car_arrow3 cars_arrow_control left_car_arrow hidden" style="display: none;"><i class="icon-angle-left"></i></span></h3>';
		$out .= '<div class="carousel">';
			$out .= '<div class="slides list_carousel responsive clearfix">';
			$out .= '<div class="team_cars">';
				while ( $wp_query->have_posts() ) : $wp_query->the_post();
				$get_meta = get_post_custom($post->ID);
				$out .= '<div class="the_portfolio_list_li_div" id="post-' . get_the_ID() . '">';
				$out .= '<div class="portfolio_item team_item">';
					$out .= '<div class="portfolio_thumbnail">';
						$out .= asalah_get_blog_thumb("370","240");

					$out .='</div>';
					$out .= '<div class="portfolio_info">';
						$out .='<h4>'.get_the_title().'</h4>';
						if ($get_meta["asalah_team_position"][0] != "" ):
						$out .= '<div class="portfolio_time">'.$get_meta["asalah_team_position"][0].'</div>';
						endif;
						
						if ($get_meta["asalah_team_fb"][0] != "" || $get_meta["asalah_team_tw"][0] != "" || $get_meta["asalah_team_gp"][0] != "" || $get_meta["asalah_team_linked"][0] != "" || $get_meta["asalah_team_pin"][0] != "" || $get_meta["asalah_team_mail"][0] != "") {
							
						$out .= '<div class="team_social_bar clearfix">';
							$out .= '<ul class="team_social_list">';
								if ($get_meta["asalah_team_fb"][0] != "") {
									$out .= '<li><a href="'.$get_meta["asalah_team_fb"][0].'"><i class="icon-facebook" title="Facebook"></i></a></li>';
								}
								if ($get_meta["asalah_team_tw"][0] != "" ) {
									$out .= '<li><a href="'. $get_meta["asalah_team_tw"][0].'"><i class="icon-twitter" title="Twitter"></i></a></li>';
								}
								if ($get_meta["asalah_team_gp"][0] != "" ) {
									$out .='<li><a href="'.$get_meta["asalah_team_gp"][0].'"><i class="icon-gplus" title="Google Plus"></i></a></li>';
								}
								if ($get_meta["asalah_team_linked"][0] != "" ) {
									$out .= '<li><a href="'.$get_meta["asalah_team_linked"][0].'"><i class="icon-linkedin" title="Linkedin"></i></a></li>';
								}
								if ($get_meta["asalah_team_pin"][0] != "" ) {
									$out .= '<li><a href="'.$get_meta["asalah_team_pin"][0].'"><i class="icon-pinterest" title="Pinterest"></i></a></li>';
								}
								if ($get_meta["asalah_team_mail"][0] != "" ) {
									$out .='<li><a href="mailto:'.$get_meta["asalah_team_mail"][0].'"><i class="icon-mail" title="Mail"></i></a></li>';
								}
							$out .='</ul>';
						$out .='</div>';
					   }
					   
					$out .='</div>';
				$out .= '</div>';
				$out .= '</div>';
				endwhile;
			$out .= '</div>';
			$out .= '</div>';
		$out .= '</div>';
	$out .= '</div>';
	
	$out .='<script type="text/javascript" language="javascript">';
	$out .= 'jQuery(window).load(function() {';
	$out .= 'jQuery("#'.$block_id.' .team_cars").carouFredSel({';
		$out .= 'responsive: true,';
		$out .= 'prev: "#'.$block_id.' .left_car_arrow3",';
		$out .= 'next: "#'.$block_id.' .right_car_arrow3",';
		$out .= 'auto: false,';
		$out .= 'swipe: {';
			$out .= 'onTouch: true,';
		$out .= '},';
		$out .= 'items: {';
			$out .= 'visible: {';
				$out .= 'min: 1,';
				$out .= 'max: ' . $max;
			$out .= '}';
		$out .= '}';
	$out .= '});';
	$out .= '});';
	$out .= '</script>';
	return $out;
	wp_reset_query();
}
add_shortcode('team', 'asalah_shortcode_team');

## Projects -------------------------------------------------- #
function asalah_shortcode_projects( $atts, $content = null ) {
    @extract($atts);
	//title
	//number
	$title = ($title) ? ' '.$title : __('Projects', 'asalah');
	$number = ($number) ? ' '.$number : '4';
	$max = ($max) ? ' '.$max : '4';
	$url = ($url) ? ' '.$url : '';
	$block_id = "project_car_".random_id(25);
	$number;
	$desc = ""; 
	$cycle ="";
	$wp_query = new WP_Query(array('post_type' => 'project', 'posts_per_page' => $number));
	
	
	$out = '<div class="portfolio_carousel" id="'. $block_id.'">';
		$out .= '<h3 class="page-header">';
			if ($url == ''):
			$out .= $title;
			else:
			$out .= '<a href="'. $url.'" title="'. $title.'">'.$title.'</a>';
			endif;
			$out .='<span class="right_car_arrow1 cars_arrow_control right_car_arrow"><i class="icon-angle-right"></i></span><span class="left_car_arrow1 cars_arrow_control left_car_arrow"><i class="icon-angle-left"></i></span></h3>';
		$out .= '<div class="carousel">';
			$out .= '<div class="slides list_carousel responsive clearfix">';
			$out .= '<div class="portfolio_cars">';
				while ( $wp_query->have_posts() ) : $wp_query->the_post();
				$out .= '<div class="the_portfolio_list_li_div" id="post-'.get_the_ID().'">';
				$out .= '<div class="portfolio_item">';
				$out .= get_asalah_slideshow();
					$out .= '<div class="portfolio_thumbnail">';
						$out .= asalah_get_blog_thumb("370","240");
						$out .= '<div class="portfolio_overlay">';
						$out .= '</div>';
						$out .= '<div class="center-bar">';
							$out .= '<a class="prettyPhotolink icon-search goup" rel="slideshow_'. get_the_ID() .'"></a>';
						$out .='</div>';
						
					$out .= '</div>';
					$out .= '<div class="portfolio_info">';
						$out .= '<a href="'.get_permalink().'"><h4>'. get_the_title().'</h4></a>';
						$tags_list = get_the_term_list( $post->ID, 'tagportfolio', '',', ','');
						$out .= '<span class="portfolio_category">'.$tags_list.'</span>';
					$out .= '</div>';
				$out .= '</div>';
				$out .= '</div>';
				endwhile;
			$out .= '</div>';
			$out .= '</div>';
		$out .= '</div>';
	$out .= '</div>';
	
	$out .='<script type="text/javascript" language="javascript">';
	$out .= 'jQuery(window).load(function() {';
	$out .= 'jQuery("#'.$block_id.' .portfolio_cars").carouFredSel({';
		$out .= 'responsive: true,';
		$out .= 'prev: "#'.$block_id.' .left_car_arrow1",';
		$out .= 'next: "#'.$block_id.' .right_car_arrow1",';
		$out .= 'auto: false,';
		$out .= 'swipe: {';
			$out .= 'onTouch: true,';
		$out .= '},';
		$out .= 'items: {';
			$out .= 'visible: {';
				$out .= 'min: 1,';
				$out .= 'max: ' . $max;
			$out .= '}';
		$out .= '}';
	$out .= '});';
	$out .= '});';
	$out .= '</script>';
	return $out;
	wp_reset_query();
}
add_shortcode('projects', 'asalah_shortcode_projects');

## Clients -------------------------------------------------- #
function asalah_shortcode_clients( $atts, $content = null ) {
    @extract($atts);
	//title
	//number
	$title = ($title) ? ' '.$title : __('Clients', 'asalah');
	$number = ($number) ? ' '.$number : '6';
	
	$block_id = "clients_car_".random_id(25);
	$url ="";
	$desc = ""; 
	$max = "4"; 
	$cycle ="";
	$wp_query = new WP_Query(array('post_type' => 'Client', 'posts_per_page' => $number));
	
	$out = '<div class="clients_content team_carousel" id="'. $block_id.'">';
		$out .= '<h3 class="page-header">'.$title.'<span class="right_car_arrow3 cars_arrow_control right_car_arrow hidden" style="display: none;"><i class="icon-angle-right"></i></span><span class="left_car_arrow3 cars_arrow_control left_car_arrow hidden" style="display: none;"><i class="icon-angle-left"></i></span></h3>';
		$out .= '<div class="clients_box ">';
			$out .= '<ul class="clients_list">';
			while ( $wp_query->have_posts() ) : $wp_query->the_post();
			$out .= '<li><a style="display:block" href="#" class="post-tooltip tooltip-n" original-title="'. get_the_title().'"><div class="client_item clearfix" style="position: relative;">'.get_client_logo().'</div></a></li>';
			endwhile;
			$out .= '</ul>';
		$out .= '</div>';
	$out .= '</div>';
	
	
	$out .='<script type="text/javascript" language="javascript">';
	$out .= 'jQuery(window).load(function() {';
	$out .= 'jQuery("#'.$block_id.' .clients_list").carouFredSel({';
		$out .= 'responsive: true,';
		$out .= 'prev: "#'.$block_id.' .left_car_arrow3",';
		$out .= 'next: "#'.$block_id.' .right_car_arrow3",';
		$out .= 'auto: false,';
		$out .= 'height: "100%",';
		$out .= 'swipe: {';
			$out .= 'onTouch: true,';
		$out .= '},';
		$out .= 'items: {';
			$out .= 'width: 160,';
			$out .= 'visible: {';
				$out .= 'min: 1,';
				$out .= 'max: 6';
			$out .= '}';
		$out .= '}';
	$out .= '});';
	$out .= '});';
	$out .= '</script>';
	return $out;
}
add_shortcode('clients', 'asalah_shortcode_clients');

## Testimonials -------------------------------------------------- #
function asalah_shortcode_testimonials( $atts, $content = null ) {
    @extract($atts);
	//title
	//number
	$title = ($title) ? $title : __('Testimonials', 'asalah');
	$number = ($number) ? $number : '3';
	
	$block_id = "testimonials_car_".random_id(25);
	$url ="";
	$number;
	$desc = ""; 
	$max = "4"; 
	$cycle ="";
	$wp_query = new WP_Query(array('post_type' => 'testimonial', 'posts_per_page' => $number));
	
	
	$out = '<div class="testimonial_content carousel list_carousel responsive clearfix" id="'. $block_id.'">';
	$out .= '<h3 class="page-header"><span class="page_header_title">'.$title.'</span><span class="right_car_arrow3 cars_arrow_control right_car_arrow hidden" style="display: none;"><i class="icon-angle-right"></i></span><span class="left_car_arrow3 cars_arrow_control left_car_arrow hidden" style="display: none;"><i class="icon-angle-left"></i></span></h3>';
		$out .= '<ul class="testy_carousel clearfix">';
			while ( $wp_query->have_posts() ) : $wp_query->the_post();
			$out .= '<li class="clearfix">';
				$out .= '<div class="testimonial_box ">';
					$out .= '<p>'.get_the_content().'</p>';
				$out .= '</div>';
				$out .= '<a target="_blank" href="'. get_testimonial_url().'">';
				$out .= '<div class="testimonials_author clearfix">';
					
					if ( has_post_thumbnail(get_the_ID()) ){
						$image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
						
						$out .= get_the_post_thumbnail('');
					}
					$out .= '<div class="tetimonials_namejob clearfix">';
					$out .= '<span class="testimonial_name">'.get_testimonial_author().'</span><span class="testimonial_job">'. get_testimonial_job().'</span>';
					$out .= '</div>';
				$out .= '</div>';
				$out .= '</a>';
			$out .= '</li>';
			endwhile;
		$out .= '</ul>';
	$out .= '</div>';	
	
	$out .='<script type="text/javascript" language="javascript">';
	$out .= 'jQuery(window).load(function() {';
	$out .= 'jQuery("#'.$block_id.' .testy_carousel").carouFredSel({';
		$out .= 'responsive: true,';
		$out .= 'prev: "#'.$block_id.' .left_car_arrow3",';
		$out .= 'next: "#'.$block_id.' .right_car_arrow3",';
		$out .= 'auto: false,';
		$out .= 'scroll: {';
			$out .= 'fx: "cover-fade",';
		$out .= '},';
		$out .= 'swipe: {';
			$out .= 'onTouch: true,';
		$out .= '},';
		$out .= 'items: {';
			$out .= 'visible: {';
				$out .= 'min: 1,';
				$out .= 'max: 1';
			$out .= '}';
		$out .= '}';
	$out .= '});';
	$out .= '});';
	$out .= '</script>';
	return $out;
	
	asalah_testimonials_carousel($the_id,$number,$title);
}
add_shortcode('testimonials', 'asalah_shortcode_testimonials');


## Tooltip -------------------------------------------------- #
function asalah_shortcode_Tooltip( $atts, $content = null ) {
    @extract($atts);
	if( empty($direction) ) $direction = 'n';
	$out = '<span class="post-tooltip tooltip-'.$direction.'" title="'.$text.'">'.$content.'</span>';
   return $out;
}
add_shortcode('tooltip', 'asalah_shortcode_Tooltip');

## Boxes -------------------------------------------------- #
function asalah_shortcode_box( $atts, $content = null ) {
    @extract($atts);
	// type : download, warning, info, shadow, success
	$type =  ($type)  ? ' '.$type  :'shadow' ;
	$align = ($align) ? ' '.$align : '';
	$class = ($class) ? ' '.$class : '';
	$width = ($width) ? ' style="width:'.$width.'"' : '';

	$out = '<div class="box'.$type.$class.$align.'"'.$width.'><div>
			' .do_shortcode($content). '
			</div></div>';
    return $out;
}
add_shortcode('box', 'asalah_shortcode_box');


## Alerts --------------------------------------------------#

function asalah_shortcode_alert($atts, $content = null) {
    @extract($atts);
	// type : alert-success, alert-block, alert-info
	$type =  ($type)  ? ' '.$type  :'shadow' ;
	$class = ($class) ? ' '.$class : '';
    
	$out = '<div class="alert '.$type.'" "' . $class.'">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<h4></h4>' .do_shortcode($content). '</div>';
	
	return $out;
}
add_shortcode('alert', 'asalah_shortcode_alert');
  

## Toggle -------------------------------------------------- #
function asalah_shortcode_Toggle( $atts, $content = null ) {
    @extract($atts);
	//state : open
	if( !isset($GLOBALS['current_collapse']) )
      $GLOBALS['current_collapse'] = 0;
    else 
      $GLOBALS['current_collapse']++;
	  
	$state =  ($state)  ? $state  :'' ;
	$title = ($title) ? $title : '';
	
	if ($state == "open") { $state = " in"; }
	$output .= '<div class="toggle-group">';
		$output .= '<div class="accordion-heading">';
			$output .= '<a class="accordion-toggle" data-toggle="collapse" href="#collapse_' . $GLOBALS['current_collapse'] . '">';
				$output .= do_shortcode( $title );
			$output .= '</a>';
		$output .= '</div>';
		$output .= '<div id="collapse_' . $GLOBALS['current_collapse'] . '" class="accordion-body collapse ' . $state . '">';
			$output .= '<div class="accordion-inner">';
				$output .= do_shortcode($content);
			$output .= '</div>';
		$output .= '</div>';
	$output .= '</div>';
	
	return $output;
}
add_shortcode('toggle', 'asalah_shortcode_Toggle');


## Buttons -------------------------------------------------- #
function asalah_shortcode_button( $atts, $content = null ) {
    @extract($atts);
	//size: small, medium, big
	// color: primary, red, orange, blue, green, black, gray, white, pink, purple
	// target: _blank
	$size  = ($size)  ? ' '.$size  :' small' ;
	$color = ($color) ? ' '.$color : ' primary';
	$link  = ($link) ? ' '.$link : '';
	$target = ($target) ? ' target="_blank"' : '';

	$out = '<a href="'.$link.'"'.$target.' class="button'.$size.$color.$align.'">' .do_shortcode($content). '</a>';
    return $out;
}
add_shortcode('button', 'asalah_shortcode_button');


## Google Map -------------------------------------------------- #
function asalah_shortcode_googlemap( $atts, $content = null ) {
    @extract($atts);
	// src
	$width  = ($width)  ? $width  :'100%' ;
	$height = ($height) ? $height : '440';

	return asalah_google_maps( $src , $width, $height );
}
add_shortcode('googlemap', 'asalah_shortcode_googlemap');



## is_logged_in shortcode -------------------------------------------------- #
function asalah_shortcode_is_logged_in( $atts, $content = null ) {
	global $user_ID ;
	if( $user_ID )
		return do_shortcode($content) ;
}
add_shortcode('is_logged_in', 'asalah_shortcode_is_logged_in');


## is_guest shortcode -------------------------------------------------- #
function asalah_shortcode_is_guest( $atts, $content = null ) {
	global $user_ID ;
	if( !$user_ID  )
		return do_shortcode($content) ;
}
add_shortcode('is_guest', 'asalah_shortcode_is_guest');


## Follow Twitter -------------------------------------------------- #
function asalah_shortcode_follow( $atts, $content = null ) {
	//id
	// count = true, false
	// size: large
    @extract($atts);

	if($size == "large") $size = 'data-size="large"' ;
		else $size="";
		
	if($count == "true") $count = "true" ;
	else $count = "false" ;

	$out = '
	<a href="https://twitter.com/'. $id .'" class="twitter-follow-button" data-show-count="'.$count.'" '.$size.'>Follow @'. $id .'</a>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';       

    return $out;
}
add_shortcode('follow', 'asalah_shortcode_follow');




## AddVideo -------------------------------------------------- #
function asalah_shortcode_AddVideo( $atts, $content = null ) {
    @extract($atts);


	$video_url = @parse_url($content);

	if ( $video_url['host'] == 'www.youtube.com' || $video_url['host']  == 'youtube.com' ) {
		parse_str( @parse_url( $content, PHP_URL_QUERY ), $my_array_of_vars );
		$video =  $my_array_of_vars['v'] ;
		$out ='<div class="video_fit_container"><iframe width="'.$width.'" height="'.$height.'" src="http://www.youtube.com/embed/'.$video.'?wmode=transparent&wmode=opaque" frameborder="0" allowfullscreen></iframe></div>';
	}
	elseif( $video_url['host'] == 'www.vimeo.com' || $video_url['host']  == 'vimeo.com' ){
		$video = (int) substr(@parse_url($content, PHP_URL_PATH), 1);
		$out='<div class="video_fit_container"> <iframe src="http://player.vimeo.com/video/'.$video.'" width="'.$width.'" height="'.$height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>';
	}
		
    return $out;
}
add_shortcode('video', 'asalah_shortcode_AddVideo');


## Facebook Subscribe-------------------------------------------------- #
function asalah_facebooksub_shortcode( $atts, $content = null ) { 
	@extract($atts);
	$username  = ($username)  ? $username  :'' ;
	$url = 'http://graph.facebook.com/' .  $username;

	return '<div class="facebook_counter social_counter clearfix">
                <a href="https://www.facebook.com/'.$username.'" class="social_counter_link">
                    <span class="counter_icon facebook_counter_icon"><i class="icon-facebook"></i></span>
                    <strong class="counter_number">'. json_decode(file_get_contents($url))->{'likes'}.'</strong>
                    <span class="counter_users_word">'. __("fans", "asalah").'</span>
                </a>
            </div>';
}  
add_shortcode("facebook_subscribe", "asalah_facebooksub_shortcode");

## Twitter Subscribe-------------------------------------------------- #
function asalah_twittersub_shortcode( $atts, $content = null ) { 
	@extract($atts);
	$username  = ($username)  ? $username  :'' ;
	$url = "http://twitter.com/users/show/" . $username;
	$response = file_get_contents ( $url );
	$t_profile = new SimpleXMLElement ( $response );
	$count = $t_profile->followers_count;
	return '<div class="twitter_counter social_counter clearfix">
                <a href="https://twitter.com/'.$username.'" class="social_counter_link">
                    <span class="counter_icon twitter_counter_icon"><i class="icon-twitter"></i></span>
                    <strong class="counter_number">'.$count.'</strong>
                    <span class="counter_users_word">'. __("Followers", "asalah").'</span>
                </a>
            </div>';
			
}  
add_shortcode("twitter_subscribe", "asalah_twittersub_shortcode");

## RSS Subscribe-------------------------------------------------- #
function asalah_rsssub_shortcode( $atts, $content = null ) { 
	@extract($atts);
	$url  = ($url)  ? $url  :'' ;
			
	return '<div class="rss_counter social_counter clearfix">
                <a href="' . $url .'" class="social_counter_link">
                    <span class="counter_icon rss_counter_icon"><i class="icon-rss"></i></span>
                    <strong class="counter_number">'. __("Subscribe", "asalah") .'</strong>
                    <span class="counter_users_word">'. __("To RSS", "asalah") . '</span>
                </a>
            </div>';
			
}  
add_shortcode("rss_subscribe", "asalah_rsssub_shortcode");


## highlight -------------------------------------------------- #
function asalah_highlight_shortcode( $atts, $content = null ) {  
    return '<span class="highlight">'.$content.'</span>';  
}  
add_shortcode("highlight", "asalah_highlight_shortcode");  


## Dropcap  -------------------------------------------------- #
function asalah_dropcap_shortcode( $atts, $content = null ) {  
    return '<span class="dropcap">'.$content.'</span>';  
}  
add_shortcode("dropcap", "asalah_dropcap_shortcode");  



## checklist -------------------------------------------------- #
function asalah_checklist_shortcode( $atts, $content = null ) {  
    return '<div class="checklist">'.do_shortcode($content).'</div>';  
}  
add_shortcode("checklist", "asalah_checklist_shortcode");


## starlist -------------------------------------------------- #
function asalah_starlist_shortcode( $atts, $content = null ) {  
    return '<div class="starlist">'.do_shortcode($content).'</div>';  
}  
add_shortcode("starlist", "asalah_starlist_shortcode");


## iconlist -------------------------------------------------- #
function asalah_iconlist_shortcode( $atts, $content = null ) {
	//type : check, star, empty, finish, circle, right, hand
	extract(shortcode_atts(array(
      "type" => 'check',
    ), $atts));  
    return '<div class="asalah_list list-'.$type.'">'.do_shortcode($content).'</div>';  
}  
add_shortcode("iconlist", "asalah_iconlist_shortcode");

## Facebook -------------------------------------------------- #
function asalah_facebook_shortcode( $atts, $content = null ) { 
	global $post;
    return '<iframe src="http://www.facebook.com/plugins/like.php?href='. get_permalink($post->ID) .'&amp;layout=box_count&amp;show_faces=false&amp;width=100&amp;action=like&amp;font&amp;colorscheme=light&amp;height=65" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:50px; height:65px;" allowTransparency="true"></iframe>';  
}  
add_shortcode("facebook", "asalah_facebook_shortcode");


## Tweet -------------------------------------------------- #
function asalah_tweet_shortcode( $atts, $content = null ) { 
	global $post, $asalah_data;
    return '<a href="http://twitter.com/share" class="twitter-share-button" data-url="'. get_permalink($post->ID) .'" data-text="'. get_the_title($post->ID) .'" data-via="'. $asalah_data['asalah_tw_url'] .'" data-lang="en" data-count="vertical" >tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>';  
}  
add_shortcode("tweet", "asalah_tweet_shortcode");


## Digg -------------------------------------------------- #
function asalah_digg_shortcode( $atts, $content = null ) { 
	global $post;
    return "
	<script type='text/javascript'>
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();
</script><a class='DiggThisButton DiggMedium' href='http://digg.com/submit?url". get_permalink($post->ID) ."=&amp;title=". get_the_title($post->ID) ."'></a>";  
}  
add_shortcode("digg", "asalah_digg_shortcode");


## stumble -------------------------------------------------- #
function asalah_stumble_shortcode( $atts, $content = null ) { 
	global $post;
    return "<su:badge layout='5' location='". get_permalink($post->ID) ."'></su:badge>
<script type='text/javascript'>
  (function() {
    var li = document.createElement('script'); li.type = 'text/javascript'; li.async = true;
    li.src = 'https://platform.stumbleupon.com/1/widgets.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(li, s);
  })();
</script>";  
}  
add_shortcode("stumble", "asalah_stumble_shortcode");



## Google + -------------------------------------------------- #
function asalah_google_shortcode( $atts, $content = null ) { 
	global $post;
    return "<g:plusone size='tall'></g:plusone>
<script type='text/javascript'>
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
";  
}  
add_shortcode("google", "asalah_google_shortcode");




## Tabs -------------------------------------------------- #
function asalah_shortcode_tabs( $atts, $content = null ) {
   if( isset($GLOBALS['tabs_count']) )
      $GLOBALS['tabs_count']++;
    else
      $GLOBALS['tabs_count'] = 0;
    extract( shortcode_atts( array(
    'tabtype' => 'nav-tabs',
    'tabdirection' => '',
	'vertical' => '',
	
  ), $atts ) );
    preg_match_all( '/tab title="([^\"]+)"/i', $content, $matches, PREG_OFFSET_CAPTURE );
    
    $tab_titles = array();
    if( isset($matches[1]) ){ $tab_titles = $matches[1]; }
    
    $output = '';
	$pos_class = "";
    if ($vertical == 'true') {
		$pos_class = "vertical_tab";	
	}else{
		$pos_class = "horizontal_tab";
	}
    if( count($tab_titles) ){
      $output .= '<div class="tabbable tabs-'.$tabdirection.' '.$pos_class.'"><ul class="nav '. $tabtype .'" id="custom-tabs-'. rand(1, 100) .'">';
      
      $i = 0;
      foreach( $tab_titles as $tab ){
        if($i == 0)
          $output .= '<li class="active">';
        else
          $output .= '<li>';

        $output .= '<a href="#custom-tab-' . $GLOBALS['tabs_count'] . '-' . sanitize_title( $tab[0] ) . '"  data-toggle="tab">' . $tab[0] . '</a></li>';
        $i++;
      }
        
        $output .= '</ul>';
        $output .= '<div class="tab-content">';
        $output .= do_shortcode( $content );
        $output .= '</div></div>';
    } else {
      $output .= do_shortcode( $content );
    }
    
    return $output;
}
function asalah_shortcode_tab( $atts, $content = null ) {

    if( !isset($GLOBALS['current_tabs']) ) {
      $GLOBALS['current_tabs'] = $GLOBALS['tabs_count'];
      $state = 'active';
    } else {

      if( $GLOBALS['current_tabs'] == $GLOBALS['tabs_count'] ) {
        $state = ''; 
      } else {
        $GLOBALS['current_tabs'] = $GLOBALS['tabs_count'];
        $state = 'active';
      }
    }

    $defaults = array( 'title' => 'Tab');
    extract( shortcode_atts( $defaults, $atts ) );
    
    return '<div id="custom-tab-' . $GLOBALS['tabs_count'] . '-'. sanitize_title( $title ) .'" class="tab-pane ' . $state . '">'. do_shortcode( $content ) .'</div>';
  }
add_shortcode('tabs', 'asalah_shortcode_tabs');
add_shortcode('tab', 'asalah_shortcode_tab');


## Divider -------------------------------------------------- #
function asalah_shortcode_divider( $atts, $content = null ) {
	@extract($atts);
	
	$title = ($title) ? $title : '';
	$size = ($size) ? $size : 'h3';
	$icon = ($icon) ? $icon : 'h3';
	
	if ($title == '') {
	$out ='<hr class="bs-docs-separator">';
	}else{
	$out ='<'.$size.' class="page-header">';
		if ($icon != '') {
			$out .='<i class="divider_icon icon-'.$icon.'"></i> ';
		}
	$out .= $title;
	$out .= '</'.$size.'>';	
	}
   return $out;
            
}
add_shortcode('divider', 'asalah_shortcode_divider');

## progress -------------------------------------------------- #
function asalah_shortcode_progress( $atts, $content = null ) {
	@extract($atts);
	//pos: right, left
	$title = ($title) ? $title : '';
	$percent = ($percent) ? $percent : '';
	
	
	if ($title != '' && $percent != '') {
		$out = '<div class="skills_content">';
			$out .= '<span class="skill_title meta_title">'.$title .' '. $percent .'%</span>';
			$out .= '<div class="progress progress-striped">';
				$out .= '<div class="bar" style="width: '. $percent .'%;"></div>';
			$out .= '</div>';
		$out .= '</div>';
						
		
	}else{
		$out = '';	
	}
   return $out;
            
}
add_shortcode('progress', 'asalah_shortcode_progress');


## pullquote -------------------------------------------------- #
function asalah_shortcode_pull( $atts, $content = null ) {
	@extract($atts);
	//pos: right, left
	$pos = ($pos) ? $pos : '';
	
	if ($pos == 'left') {
	$out ='<span class="pullquote text-left">' . $content .'</span>';
	}elseif($pos == 'right'){
	$out ='<span class="pullquote text-right">' . $content .'</span>';	
	}else{
	$out ='<blockquote>' . $content .'</blockquote>';	
	}
   return $out;
            
}
add_shortcode('pull', 'asalah_shortcode_pull');

## Icons
function asalah_shortcode_icon( $atts, $content = null ) {
    extract(shortcode_atts(array(
      "type" => 'type',
      "size" => 'normal',
    ), $atts));

    return '<i class="icon icon-'.$type.'" style="font-size:'. $size .'px;"></i>'; 
  }
  
  add_shortcode('icon', 'asalah_shortcode_icon');


## Columns  -------------------------------------------------- #
function asalah_one_third( $atts, $content = null ) {
   return '<div class="one_third">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_third', 'asalah_one_third');

function asalah_one_third_last( $atts, $content = null ) {
   return '<div class="one_third last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}
add_shortcode('one_third_last', 'asalah_one_third_last');

function asalah_two_third( $atts, $content = null ) {
   return '<div class="two_third">' . do_shortcode($content) . '</div>';
}
add_shortcode('two_third', 'asalah_two_third');

function asalah_two_third_last( $atts, $content = null ) {
   return '<div class="two_third last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}
add_shortcode('two_third_last', 'asalah_two_third_last');

function asalah_one_half( $atts, $content = null ) {
   return '<div class="one_half">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_half', 'asalah_one_half');

function asalah_one_half_last( $atts, $content = null ) {
   return '<div class="one_half last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}
add_shortcode('one_half_last', 'asalah_one_half_last');

function asalah_one_fourth( $atts, $content = null ) {
   return '<div class="one_fourth">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_fourth', 'asalah_one_fourth');

function asalah_one_fourth_last( $atts, $content = null ) {
   return '<div class="one_fourth last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}
add_shortcode('one_fourth_last', 'asalah_one_fourth_last');

function asalah_three_fourth( $atts, $content = null ) {
   return '<div class="three_fourth">' . do_shortcode($content) . '</div>';
}
add_shortcode('three_fourth', 'asalah_three_fourth');

function asalah_three_fourth_last( $atts, $content = null ) {
   return '<div class="three_fourth last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}
add_shortcode('three_fourth_last', 'asalah_three_fourth_last');

function asalah_one_fifth( $atts, $content = null ) {
   return '<div class="one_fifth">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_fifth', 'asalah_one_fifth');

function asalah_one_fifth_last( $atts, $content = null ) {
   return '<div class="one_fifth last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}
add_shortcode('one_fifth_last', 'asalah_one_fifth_last');

function asalah_two_fifth( $atts, $content = null ) {
   return '<div class="two_fifth">' . do_shortcode($content) . '</div>';
}
add_shortcode('two_fifth', 'asalah_two_fifth');

function asalah_two_fifth_last( $atts, $content = null ) {
   return '<div class="two_fifth last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}
add_shortcode('two_fifth_last', 'asalah_two_fifth_last');

function asalah_three_fifth( $atts, $content = null ) {
   return '<div class="three_fifth">' . do_shortcode($content) . '</div>';
}
add_shortcode('three_fifth', 'asalah_three_fifth');

function asalah_three_fifth_last( $atts, $content = null ) {
   return '<div class="three_fifth last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}
add_shortcode('three_fifth_last', 'asalah_three_fifth_last');

function asalah_four_fifth( $atts, $content = null ) {
   return '<div class="four_fifth">' . do_shortcode($content) . '</div>';
}
add_shortcode('four_fifth', 'asalah_four_fifth');

function asalah_four_fifth_last( $atts, $content = null ) {
   return '<div class="four_fifth last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}
add_shortcode('four_fifth_last', 'asalah_four_fifth_last');

function asalah_one_sixth( $atts, $content = null ) {
   return '<div class="one_sixth">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_sixth', 'asalah_one_sixth');

function asalah_one_sixth_last( $atts, $content = null ) {
   return '<div class="one_sixth last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}
add_shortcode('one_sixth_last', 'asalah_one_sixth_last');

function asalah_five_sixth( $atts, $content = null ) {
   return '<div class="five_sixth">' . do_shortcode($content) . '</div>';
}
add_shortcode('five_sixth', 'asalah_five_sixth');

function asalah_five_sixth_last( $atts, $content = null ) {
   return '<div class="five_sixth last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}
add_shortcode('five_sixth_last', 'asalah_five_sixth_last');
?>