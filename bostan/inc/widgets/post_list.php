<?php
add_action( 'widgets_init', 'postlist_widget_init' );
function postlist_widget_init() {
	register_widget( 'postlist_widget' );
}

class postlist_widget extends WP_Widget {
	function postlist_widget() {
			$widget_ops = array( 'classname' => 'postlist-widget', 'description' => ''  );
			$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'postlist-widget' );
			$this->WP_Widget( 'postlist-widget',theme_name .' - Posts List', $widget_ops, $control_ops );
		}
	
	function widget( $args, $instance ) {
		extract( $args );

		$title = apply_filters('widget_title', $instance['title'] );
		$type = $instance['type'];
		$number = $instance['number'];
		$order = $instance['order'];
			
		echo $before_widget;
		
		if ( $title ) :
			echo $before_title;
			echo $title ; 
			echo $after_title; 
		endif;
			?>
        	<?php
			asalah_posts_list($type, $number, $order)
			?>
		<?php 
        echo $after_widget;
    }
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['type'] = $new_instance['type'] ;
		$instance['number'] = $new_instance['number'] ;
		$instance['order'] = $new_instance['order'] ;
		return $instance;
	}
	
	function form( $instance ) {
		$defaults = array( 'title' =>__('Post List', 'asalah') );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title', 'asalah'); ?>: </label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" type="text" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'type' ); ?>"><?php _e('Posts Type', 'asalah'); ?>: </label>
			<select id="<?php echo $this->get_field_id( 'type' ); ?>" name="<?php echo $this->get_field_name( 'type' ); ?>" >
				<option value="post" <?php if( $instance['type'] == 'post' ) echo "selected=\"selected\""; else echo ""; ?>><?php _e('Posts', 'asalah'); ?></option>
				<option value="project" <?php if( $instance['type'] == 'project' ) echo "selected=\"selected\""; else echo ""; ?>><?php _e('Projects', 'asalah'); ?></option>
			</select>
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e('Number Of Posts', 'asalah'); ?>: </label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" type="text" size="3" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'style' ); ?>"><?php _e('Post Order', 'asalah'); ?>: </label>
			<select id="<?php echo $this->get_field_id( 'style' ); ?>" name="<?php echo $this->get_field_name( 'style' ); ?>" >
				<option value="date" <?php if( $instance['style'] == 'date' ) echo "selected=\"selected\""; else echo ""; ?>><?php _e('Date', 'asalah'); ?></option>
				<option value="comment_count" <?php if( $instance['style'] == 'comment_count' ) echo "selected=\"selected\""; else echo ""; ?>><?php _e('Comment Count', 'asalah'); ?></option>
                <option value="rand" <?php if( $instance['style'] == 'rand' ) echo "selected=\"selected\""; else echo ""; ?>><?php _e('Random', 'asalah'); ?></option>
			</select>
		</p>
	<?php
	}
}


?>