<?php
add_action('widgets_init', 'subscribe_widget_init');

function subscribe_widget_init() {
    register_widget('subscribe_widget');
}

class subscribe_widget extends WP_Widget {

    function subscribe_widget() {
        $widget_ops = array('classname' => 'subscribe-widget', 'description' => '');
        $control_ops = array('width' => 250, 'height' => 350, 'id_base' => 'subscribe-widget');
        $this->WP_Widget('subscribe-widget', theme_name . ' - subscribe', $widget_ops, $control_ops);
    }

    function widget($args, $instance) {
        extract($args);

        $title = apply_filters('widget_title', $instance['title']);
        $fb = $instance['fb'];
        $tw = $instance['tw'];
        $rss = $instance['rss'];

        echo $before_widget;
        if ($title) :
            echo $before_title;
            echo $title;
            echo $after_title;
        endif;
        ?>


        <div class="row-fluid">
        <?php if ($fb) { ?>
            <?php $url = 'http://graph.facebook.com/' . $fb; ?>
                <div class="facebook_counter social_counter clearfix">
                    <a href="https://www.facebook.com/<?php echo $fb; ?>" class="social_counter_link">
                        <span class="counter_icon facebook_counter_icon"><i class="icon-facebook"></i></span>
                        <strong class="counter_number"><?php echo json_decode(file_get_contents($url))->{'likes'}; ?></strong>
                        <span class="counter_users_word"><?php _e("fans", "asalah"); ?></span>
                    </a>
                </div>
            <?php } ?>
            
            <?php if ($tw) { ?>
                <?php
                
                $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
                $getfield = '?screen_name='.$tw;
                $requestMethod = 'GET';
                $twitter = new TwitterAPIExchange($settings);
                $follow_count=$twitter->setGetfield($getfield)
                             ->buildOauth($url, $requestMethod)
                             ->performRequest();
                            $testCount = json_decode($follow_count, true);
                ?>
                <div class="twitter_counter social_counter clearfix">
                    <a href="https://twitter.com/<?php echo $tw; ?>" class="social_counter_link">
                        <span class="counter_icon twitter_counter_icon"><i class="icon-twitter"></i></span>
                        <strong class="counter_number"><?php echo $testCount[0]['user']['followers_count']; ?></strong>
                        <span class="counter_users_word"><?php _e("Followers", "asalah"); ?></span>
                    </a>
                </div>
            <?php } ?>

            <?php if ($rss) { ?>
                <div class="rss_counter social_counter clearfix">
                    <a href="<?php echo $tw; ?>" class="social_counter_link">
                        <span class="counter_icon rss_counter_icon"><i class="icon-rss"></i></span>
                        <strong class="counter_number"><?php _e("Subscribe", "asalah"); ?></strong>
                        <span class="counter_users_word"><?php _e("To RSS", "asalah"); ?></span>
                    </a>
                </div>
            <?php } ?>
        </div>


        <?php
        echo $after_widget;
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['fb'] = $new_instance['fb'];
        $instance['tw'] = $new_instance['tw'];
        $instance['rss'] = $new_instance['rss'];
        return $instance;
    }

    function form($instance) {
        $defaults = array('title' => __('Subscribe', 'asalah'));
        $instance = wp_parse_args((array) $instance, $defaults);
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'asalah'); ?>: </label>
            <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" class="widefat" type="text" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('fb'); ?>"><?php _e('Facebook Page ID/Username', 'asalah'); ?>: </label>
            <input id="<?php echo $this->get_field_id('fb'); ?>" name="<?php echo $this->get_field_name('fb'); ?>" value="<?php echo $instance['fb']; ?>" class="widefat" type="text" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('tw'); ?>"><?php _e('Twitter Profile Username', 'asalah'); ?>: </label>
            <input id="<?php echo $this->get_field_id('tw'); ?>" name="<?php echo $this->get_field_name('tw'); ?>" value="<?php echo $instance['tw']; ?>" class="widefat" type="text" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('rss'); ?>"><?php _e('RSS URL', 'asalah'); ?>: </label>
            <input id="<?php echo $this->get_field_id('rss'); ?>" name="<?php echo $this->get_field_name('rss'); ?>" value="<?php echo $instance['rss']; ?>" class="widefat" type="text" />
        </p>

        <?php
    }

}
?>