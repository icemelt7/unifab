<?php  
/* 
Template Name: Portfolio 3 Culomns
*/  
?>
<?php get_header(); ?>
<!-- post title holder -->
<?php if (get_post_meta($post->ID, 'asalah_custom_title_bg', true)): ?>
<style>
.page_title_holder {
    background-image: url('<?php echo get_post_meta($post->ID, 'asalah_custom_title_bg', true);  ?>');
    background-repeat: no-repeat;
}    
</style>
<?php endif; ?>
<div class="page_title_holder container-fluid">
	<div class="container">
		<div class="page_info">
			<h1><?php the_title(); ?></h1>
			<?php asalah_breadcrumbs(); ?>
		</div>
		<div class="page_nav">
		
		</div>
	</div>
</div>
<?php  
	$post_number = 12;
	if ($asalah_data['asalah_portfolio_number']) {
		$post_number = $asalah_data['asalah_portfolio_number'];	
	}
	$wp_query = new WP_Query(array('post_type' => 'project', 'paged' => get_query_var( 'paged' ), 'posts_per_page' => $post_number));  
	$count =0;  
?>  
<!-- end post title holder -->
<section class="main_content">
	<!-- start single portfolio container -->
	<?php if ( $wp_query ) : ?>
	<div class="container services new_section">
		<div id="post-<?php the_ID(); ?>" <?php post_class('portfolio_page row'); ?>>
			<nav id="portfolio_filter_options" class="span12 navbar">
				
				<div class="navbar-inner">
				<i class="brand icon-search" href="#"></i>
				<?php asalah_portfolio_tag_list(); ?>
				</div>
			</nav>
			<div class="span12 portfolio_items">
				<ul div id="portfolio_container" class="thumbnails">
					<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
						<li class="portfolio_element span4 <?php asalah_portfolio_tag(); ?>">
							<div class="portfolio_item">
                            <?php asalah_slideshow(); ?>
								<div class="portfolio_thumbnail">
									<?php asalah_blog_thumb("720","518") ?>
									<div class="portfolio_overlay">
                                    </div>
                                    <div class="center-bar">
                                        <a class="prettyPhotolink icon-search goup" rel="slideshow_<?php echo $post->ID; ?>"></a>
                                    </div>
								</div>
								<div class="portfolio_info">
									<a href="<?php the_permalink(); ?>"><h5><?php the_title(); ?></h5></a>
                            		<span class="portfolio_category"><?php $tags_list = get_the_term_list( $post->ID, 'tagportfolio', '',', ',''); echo $tags_list; ?></span>
								</div>
							</div>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php asalah_bootstrap_pagination(); ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<!-- end single portfolio container -->


<?php get_footer(); ?>