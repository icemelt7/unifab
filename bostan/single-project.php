<?php get_header(); ?>
<!-- post title holder -->
<?php if (get_post_meta($post->ID, 'asalah_custom_title_bg', true)): ?>
    <style>
        .page_title_holder {
            background-image: url('<?php echo get_post_meta($post->ID, 'asalah_custom_title_bg', true); ?>');
            background-repeat: no-repeat;
        }    
    </style>
<?php endif; ?>
<div class="page_title_holder container-fluid">
    <div class="container">
        <div class="page_info">
            <h1><?php the_title(); ?></h1>
            <?php asalah_breadcrumbs(); ?>

        </div>
        <div class="page_nav clearfix">
            <?php
            $next_post = get_next_post();
            $prev_post = get_previous_post();
            ?>

            <?php if (!empty($prev_post)): ?>
                <a title="<?php _e("Next Project", 'asalah'); ?>" href="<?php echo get_permalink($prev_post->ID); ?>" id="right_nav_arrow" class="cars_nav_control right_nav_arrow"><i class="icon-angle-right"></i></a>
            <?php endif; ?>

            <?php if ($asalah_data['asalah_portfolio_url']): ?>
                <a href="<?php echo $asalah_data['asalah_portfolio_url']; ?>" id="main_portfolio_arrow" class="cars_portfolio_control"><i class="icon-th-large"></i></a>
            <?php endif; ?>

            <?php if (!empty($next_post)): ?>
                <a title="<?php _e("Previous Project", 'asalah'); ?>" href="<?php echo get_permalink($next_post->ID); ?>" id="left_nav_arrow" class="cars_nav_control left_nav_arrow"><i class="icon-angle-left"></i></a>
            <?php endif; ?>
        </div>
    </div>
</div>
<!-- end post title holder -->
<section class="main_content">
    <!-- start single portfolio container -->
    <?php while (have_posts()) : the_post(); ?>
        <div class="container new_section">
            <?php if (get_post_meta($post->ID, 'asalah_post_layout', true) != 'full'): ?>
                <div id="post-<?php the_ID(); ?>" <?php post_class('project_post row-fluid'); ?>>
                    <div class="span8 portfolio_banner blog_main_content <?php if (get_post_meta($post->ID, 'asalah_post_layout', true) == 'left') {
            echo " pull-right";
        } ?>">
        <?php asalah_banner(); ?>
                    </div>
                    <div class="span4 portfolio_details_content side_content <?php if (get_post_meta($post->ID, 'asalah_post_layout', true) == 'left') {
            echo " pull-left";
        } ?>">
                        <div class="new_content">
                            <div class="portfolio_section_title"><h4 class="page-header"><span class="page_header_title"><?php asalah_translat('asalah_translate_projectoverview', "Project Overview") ?></span>


                                </h4></div>
                            <div class="portfolio_section_title">
        <?php the_content(); ?>
        <?php if (get_post_meta($post->ID, 'project_url', true) != ''): ?>
                                    <div class="live_preview_link"><a target="_blank" href="<?php echo get_post_meta($post->ID, 'project_url', true); ?>"><?php _e("Live Preview ...", "asalah"); ?></a></div>
                        <?php endif; ?>
                            </div>
                        </div>

                                <?php if (get_post_meta($post->ID, 'asalah_post_meta_info', true) != 'hide'): ?>
                            <div class="new_content">
                                <div class="portfolio_section_title"><h4 class="page-header"><span class="page_header_title"><?php asalah_translat('asalah_translate_projectdetails', "Project Details") ?></span></h4></div>
                                <div class="portfolio_details_content">
            <?php if (get_post_meta($post->ID, 'asalah_project_client', true) != '') { ?>
                                        <div class="portfolio_details_item"><p><strong><?php _e("Client", "asalah"); ?>: </strong><?php echo get_post_meta($post->ID, 'asalah_project_client', true); ?></p></div>
                            <?php } ?>
                                    <div class="portfolio_details_item"><p><strong><?php _e("Date", "asalah"); ?>: </strong><?php echo $date = get_the_date('', $post->ID); ?></p></div>
                                    <div class="portfolio_details_item"><p><strong><?php _e("Tags", "asalah"); ?>: </strong><?php $tags_list = get_the_term_list($post->ID, 'tagportfolio', '', ', ', '');
                echo $tags_list; ?></p></div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php
                        if (get_post_meta($post->ID, 'asalah_review_pos', true) == 'bottom') {
                            asalah_project_skills_list();
                        }
                        ?>

        <?php
        if (get_post_meta($post->ID, 'asalah_post_share_box', true) == 'show'):
            asalah_post_share();
        endif;
        ?>

                    </div>
                </div>
    <?php else: ?>
                <div id="post-<?php the_ID(); ?>" <?php post_class('project_post'); ?>>
                    <div class="row-fluid new_content">
                        <div class="span12">
        <?php asalah_banner(); ?>
                        </div>
                    </div>
                    <div class="row-fluid new_content full_width_project_details">
                        <div class="span12 ">
                            <div class="new_content">
                                <div class="portfolio_section_title"><h4 class="page-header"><span class="page_header_title"><?php asalah_translat('asalah_translate_projectoverview', "Project Overview") ?></span>

                                    </h4></div>
        <?php the_content(); ?>
                            </div>
                        </div>

                                <?php if (get_post_meta($post->ID, 'asalah_post_meta_info', true) != 'hide'): ?>
                            <div class="new_content clearfix">
                                <div class="portfolio_section_title"><h4 class="page-header"><span class="page_header_title"><?php asalah_translat('asalah_translate_projectdetails', "Project Details") ?></span></h4></div>
                                <div class="portfolio_details_content">
            <?php if (get_post_meta($post->ID, 'asalah_project_client', true) != '') { ?>
                                        <div class="portfolio_details_item"><p><strong><?php _e("Client", "asalah"); ?>: </strong><?php echo get_post_meta($post->ID, 'asalah_project_client', true); ?></p></div>
                            <?php } ?>
                                    <div class="portfolio_details_item"><p><strong><?php _e("Date", "asalah"); ?>: </strong><?php echo $date = get_the_date('', $post->ID); ?></p></div>
                                    <div class="portfolio_details_item"><p><strong><?php _e("Tags", "asalah"); ?>: </strong><?php $tags_list = get_the_term_list($post->ID, 'tagportfolio', '', ', ', '');
                echo $tags_list; ?></p></div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php
                        if (get_post_meta($post->ID, 'asalah_review_pos', true) == 'bottom') {
                            asalah_project_skills_list();
                        }
                        ?>
        <?php
        if (get_post_meta($post->ID, 'asalah_post_share_box', true) == 'show'):
            asalah_post_share();
        endif;
        ?>
                    </div>
                </div>
            </div>
    <?php endif ?>



    </div>
    <?php if (get_post_meta($post->ID, 'asalah_post_other', true) == 'show'): ?>
        <div class="container-fluid another_projects new_section"><div class="container">
                <div id="anotherprojects-<?php the_ID(); ?>" class="row-fluid">
                    <div class="span12">
        <?php asalah_posts_carousel('anotherprojects-' . get_the_ID(), 'project', '', '9', "Other Projects", "", 4, "", "top", $post->ID); ?>
                    </div>
                </div>
            </div></div>
    <?php endif; ?>


<?php endwhile; ?>
<!-- end single portfolio container -->


<?php get_footer(); ?>