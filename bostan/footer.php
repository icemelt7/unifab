</section><!-- #content -->

</div> <!-- end body width from header -->
<?php global $asalah_data; ?>	

<footer class="container-fluid site_footer body_width">
	<div class="container">
		<div class="row-fluid">
        	<?php
                get_sidebar( 'footer' );
            ?>
		</div>
	</div>
</footer>

<div class="container-fluid site_secondary_footer body_width">
	<div class="container secondary_footer_container">
		<div class="row-fluid">
        	<div class="pull-left">
            <?php if ($asalah_data['asalah_copyright_text']): ?>
            	<?php echo $asalah_data['asalah_copyright_text']; ?>
            <?php endif; ?>
            </div>
		</div>
	</div>
    
    
</div>
<?php if ($asalah_data['asalah_footer_code']): ?>
		<?php echo $asalah_data['asalah_footer_code']; ?>
    <?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>