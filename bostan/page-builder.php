<?php  
/* 
Template Name: Page Builder
*/  
?>
<?php get_header(); ?>
<section class="main_content page_builder_content">
    
		<?php while ( have_posts() ) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class('blog_post'); ?>>
			<?php the_content(); ?>
		</div>
		<?php endwhile; ?>
	


<?php get_footer(); ?>