<?php
$themename = "Bostan";
$shortname = "asalah";
define('theme_name', $themename);
define('theme_ver', 1);
include (TEMPLATEPATH . '/framework/wp-pricing-table/index.php');
include (TEMPLATEPATH . '/inc/scripts.php');
include (TEMPLATEPATH . '/inc/shortcodes.php');
include (TEMPLATEPATH . '/inc/social.php');
include (TEMPLATEPATH . '/inc/banner.php');
include (TEMPLATEPATH . '/inc/blog.php');
include (TEMPLATEPATH . '/inc/slider.php');
include (TEMPLATEPATH . '/inc/eslider.php');
include (TEMPLATEPATH . '/inc/lists.php');
include (TEMPLATEPATH . '/inc/shortcodes/shortcode.php');
include (TEMPLATEPATH . '/inc/themes-style.php');
include (TEMPLATEPATH . '/framework/bootstrap/function.php');
include (TEMPLATEPATH . '/framework/aqua/aq-page-builder.php');
include (TEMPLATEPATH . '/framework/twitter/twitteroauth.php');

include (TEMPLATEPATH . '/inc/postsoptions.php');
include (TEMPLATEPATH . '/admin/index.php');

//include post types
include (TEMPLATEPATH . '/inc/portfolio.php');
include (TEMPLATEPATH . '/inc/team.php');
include (TEMPLATEPATH . '/inc/testimonials.php');
include (TEMPLATEPATH . '/inc/clients.php');

//include widgets
include (TEMPLATEPATH . '/inc/widgets/video.php');
include (TEMPLATEPATH . '/inc/widgets/soundcloud.php');
include (TEMPLATEPATH . '/inc/widgets/subscribe.php');
include (TEMPLATEPATH . '/inc/widgets/likebox.php');
include (TEMPLATEPATH . '/inc/widgets/googleplusbox.php');
include (TEMPLATEPATH . '/inc/widgets/post_list.php');
include (TEMPLATEPATH . '/inc/widgets/ads.php');
include (TEMPLATEPATH . '/inc/widgets/tweets.php');


if (isset($asalah_data['asalah_tf_username']) && $asalah_data['asalah_tf_username'] && isset($asalah_data['asalah_tf_api']) && $asalah_data['asalah_tf_api']) {

    function add_update_menu() {

        add_theme_page(theme_name . ' Update', theme_name . ' Updates', 'manage_options', 'updating', 'theadminpage');
    }

    $tfuname = $asalah_data['asalah_tf_username'];
    $tfapi = $asalah_data['asalah_tf_api'];
    add_action('admin_menu', 'add_update_menu');

    function theadminpage() {

        global $tfuname, $tfapi;

        include_once(TEMPLATEPATH . '/framework/envato-wordpress-toolkit-library/class-envato-wordpress-theme-upgrader.php');
        $upgrader = new Envato_WordPress_Theme_Upgrader($tfuname, $tfapi);

        if (isset($_POST['upgradingthemever'])) {
            $upgrader->upgrade_theme();
        }
        $currver = $upgrader->check_for_theme_update();
        ?>
        <style>.updatenotice { margin-top: 20px;}</style>
        <?php
        if ($currver->updated_themes_count) {
            ?>
            <div class="updatenotice">New Update Available</div>
            <div>
                <form method="post">
                    <input type="submit" name="upgradingthemever" value="Update Now" />
                </form>
            </div>
            <?php
        } else {
            ?>
            <div class="updatenotice">Congratulations, you are up to date :)</div>
            <?php
        }
    }

}



remove_filter('term_description', 'wpautop');

function theme_setup() {
    add_editor_style();
    load_theme_textdomain('asalah', get_template_directory() . '/languages');

    // Register primary menu.
    register_nav_menu('primarymenu', __('Primary Menu', 'asalah'));
    // Add default posts and comments RSS feed links to <head>.
    add_theme_support('automatic-feed-links');
    add_theme_support('post-thumbnails');
    add_image_size('portfolio', 520, 337, true); //(cropped)
    add_image_size('team', 500, 528, true); //(cropped)
}

add_action('after_setup_theme', 'theme_setup');

function asalah_mobile_menu($args = array()) {
    $output = '';

    @extract($args);

    if (( $locations = get_nav_menu_locations() ) && isset($locations[$menu_name])) {
        $menu = wp_get_nav_menu_object($locations[$menu_name]);
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        $output = "<select id='" . $id . "' class='" . $class . "'>";
        $output .= "<option value='' selected='selected'>" . __('Go to...', 'asalah') . "</option>";
        foreach ((array) $menu_items as $key => $menu_item) {
            $title = $menu_item->title;
            $url = $menu_item->url;

            if ($menu_item->menu_item_parent) {
                $title = ' - ' . $title;
            }
            $output .= "<option value='" . $url . "'>" . $title . '</option>';
        }
        $output .= '</select>';
    }
    return $output;
}

function asalah_widgets_init() {
    global $asalah_data;
    register_sidebar(array(
        'name' => __('Blog sidebar', 'asalah'),
        'id' => 'sidebar-blog',
        'description' => __('An optional widget area for your blog page', 'asalah'),
        'before_widget' => '<div id="%1$s" class="widget_container clearfix widget %2$s">',
        'after_widget' => "</div>",
        'before_title' => '<h3 class="page-header"><span class="page_header_title">',
        'after_title' => '</span></h3>',
    ));

    register_sidebar(array(
        'name' => __('Category page sidebar', 'asalah'),
        'id' => 'sidebar-cat',
        'description' => __('An optional widget area for your categories', 'asalah'),
        'before_widget' => '<div id="%1$s" class="widget_container clearfix widget %2$s">',
        'after_widget' => "</div>",
        'before_title' => '<h3 class="page-header"><span class="page_header_title">',
        'after_title' => '</span></h3>',
    ));

    register_sidebar(array(
        'name' => __('Single blog post sidebar', 'asalah'),
        'id' => 'sidebar-single',
        'description' => __('An optional widget area for your blog post page', 'asalah'),
        'before_widget' => '<div id="%1$s" class="widget_container clearfix widget %2$s">',
        'after_widget' => "</div>",
        'before_title' => '<h3 class="page-header"><span class="page_header_title">',
        'after_title' => '</span></h3>',
    ));

    register_sidebar(array(
        'name' => __('Single page post sidebar', 'asalah'),
        'id' => 'sidebar-page',
        'description' => __('An optional widget area for your pages', 'asalah'),
        'before_widget' => '<div id="%1$s" class="widget_container clearfix widget %2$s">',
        'after_widget' => "</div>",
        'before_title' => '<h3 class="page-header"><span class="page_header_title">',
        'after_title' => '</span></h3>',
    ));

    register_sidebar(array(
        'name' => __('Footer Area One', 'asalah'),
        'id' => 'sidebar-1',
        'description' => __('An optional widget area for your site footer', 'asalah'),
        'before_widget' => '<div id="%1$s" class="widget_container clearfix widget %2$s">',
        'after_widget' => "</div>",
        'before_title' => '<h3 class="page-header"><span class="page_header_title">',
        'after_title' => '</span></h3>',
    ));

    register_sidebar(array(
        'name' => __('Footer Area Two', 'asalah'),
        'id' => 'sidebar-2',
        'description' => __('An optional widget area for your site footer', 'asalah'),
        'before_widget' => '<div id="%1$s" class="widget_container clearfix widget %2$s">',
        'after_widget' => "</div>",
        'before_title' => '<h3 class="page-header"><span class="page_header_title">',
        'after_title' => '</span></h3>',
    ));

    register_sidebar(array(
        'name' => __('Footer Area Three', 'asalah'),
        'id' => 'sidebar-3',
        'description' => __('An optional widget area for your site footer', 'asalah'),
        'before_widget' => '<div id="%1$s" class="widget_container clearfix widget %2$s">',
        'after_widget' => "</div>",
        'before_title' => '<h3 class="page-header"><span class="page_header_title">',
        'after_title' => '</span></h3>',
    ));

    register_sidebar(array(
        'name' => __('Footer Area four', 'asalah'),
        'id' => 'sidebar-4',
        'description' => __('An optional widget area for your site footer', 'asalah'),
        'before_widget' => '<div id="%1$s" class="widget_container clearfix widget %2$s">',
        'after_widget' => "</div>",
        'before_title' => '<h3 class="page-header"><span class="page_header_title">',
        'after_title' => '</span></h3>',
    ));


    $sidebars = $asalah_data['asalah_custom_sidebars'];
    if ($sidebars):

        foreach ($sidebars as $option) {
            $siebar_id = "asalah_custom_sidebar_" . $option['order'];
            register_sidebar(array(
                'name' => $option['title'],
                'id' => $siebar_id,
                'description' => __('Custom Sidebar', 'asalah'),
                'before_widget' => '<div id="%1$s" class="widget_container clearfix widget %2$s">',
                'after_widget' => "</div>",
                'before_title' => '<h3 class="page-header"><span class="page_header_title">',
                'after_title' => '</span></h3>',
            ));
        }

    endif;
}

add_action('widgets_init', 'asalah_widgets_init');

function asalah_project_skills_list() {
    global $post;
    $get_meta = get_post_custom($post->ID);
    $skills_items = unserialize($get_meta['asalah_project_skills_item'][0]);
    $score = '';
    $count = '';
    ?>
    <div class="new_content">
        <div class="portfolio_section_title"><h4 class="page-header"><span class="page_header_title"><?php _e("Project Skills", 'asalah'); ?></span></h4></div>
        <div class="portfolio_skills_content">
            <?php
            foreach ($skills_items as $skills) {
                if ($skills['name'] && $skills['score']) {
                    ?>
                    <span class="skill_title meta_title"><?php echo $skills['name']; ?> <?php echo $skills['score']; ?>%</span>
                    <div class="progress progress-striped">
                        <div class="bar" style="width: <?php echo $skills['score']; ?>%;"></div>
                    </div>
                    <?php
                    $score += $skills['score'];
                    ;
                    $count += 1;
                }
            }
            ?>
        </div>  
    </div>
    <?php
}

if (!function_exists('asalah_content_nav')) :

    /**
     * Display navigation to next/previous pages when applicable
     */
    function asalah_content_nav() {
        global $wp_query;

        if ($wp_query->max_num_pages > 1) :
            ?>
            <div class="pagination">
                <nav class="content_nav">
                    <div class="prev_page">
            <?php next_posts_link('OLD POSTS'); ?>
                    </div>
                    <div class="next_page">
            <?php previous_posts_link('NEW POSTS'); ?>
                    </div>
                </nav>
            </div>
        <?php
        endif;
    }

endif;

if (!function_exists('asalah_comment')) :

    function asalah_comment($comment, $args, $depth) {
        $GLOBALS['comment'] = $comment;
        switch ($comment->comment_type) :
            case 'pingback' :
            case 'trackback' :
                ?>
                <li class="post pingback">
                    <p><?php _e('Pingback: ', 'asalah'); ?> <?php comment_author_link(); ?> (<?php edit_comment_link(__('Edit', 'asalah'), '<span class="edit-link">', '</span>'); ?>)</p>
                        <?php
                        break;
                    default :
                        ?>
                <li <?php comment_class("media the_comment"); ?> id="li-comment-<?php comment_ID(); ?>">
                    <a class="pull-left commenter" href="#">
                        <?php
                        $avatar_size = 80;
                        if ('0' != $comment->comment_parent)
                            $avatar_size = 80;

                        echo get_avatar($comment, $avatar_size);
                        ?>
                    </a>
                    <div class="media-body comment_body">
                        <div class="media-heading clearfix">
                            <h5 class="commenter_name"><?php echo get_comment_author_link(); ?></h5>
                            <div class="comment_info"><a href="<?php echo esc_url(get_comment_link($comment->comment_ID)); ?>"><time pubdate datetime="<?php echo get_comment_time('c'); ?>"><?php echo get_comment_date() . ' at ' . get_comment_time(); ?></time></a> - <?php comment_reply_link(array_merge($args, array('reply_text' => __('Reply', 'asalah'), 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?></div>
                        </div>
                        <?php comment_text(); ?>


                        <?php
                        break;
                endswitch;
            }

        endif;

        function asalah_google_maps($src, $width = '100%', $height = 500) {
            return '<div class="google-map"><iframe width="' . $width . '" height="' . $height . '" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="' . $src . '&amp;output=embed"></iframe></div>';
        }

        add_filter('widget_text', 'do_shortcode');

        function related_posts_list($post_id) {
            $tags = wp_get_post_tags($post_id);
            if ($tags) {
                $tag_ids = array();
                foreach ($tags as $individual_tag)
                    $tag_ids[] = $individual_tag->term_id;

                $args = array(
                    'tag__in' => $tag_ids,
                    'post__not_in' => array($post_id),
                    'showposts' => 4, // Number of related posts that will be shown.  
                    'ignore_sticky_posts' => 1
                );

                $my_query = new wp_query($args);
                while ($my_query->have_posts()) : $my_query->the_post();
                    ?>
                    <li><a href="<?php the_permalink(); ?>"><h6><?php the_title(); ?></h6></a></li>
                    <?php
                endwhile;
            }
        }

        function single_related_posts() {
            global $post;
            $tags = wp_get_post_tags($post->ID);
            if ($tags) {
                $tag_ids = array();
                foreach ($tags as $individual_tag)
                    $tag_ids[] = $individual_tag->term_id;

                $args = array(
                    'orderby' => 'rand',
                    'tag__in' => $tag_ids,
                    'post__not_in' => array($post->ID),
                    'showposts' => 2, // Number of related posts that will be shown.  
                    'caller_get_posts' => 1
                );

                $my_query = new wp_query($args);
                if ($my_query->have_posts()) {
                    ?>
                    <div class="row-fluid content_boxes">
                        <div class="single_related_articles span12">
                            <div class="page-header clearfix"><h4><?php _e('Related Articles', 'asalah'); ?></h4></div>
                            <div class="single_related_box row-fluid">
            <?php
            while ($my_query->have_posts()) : $my_query->the_post();
                $date = get_the_date('', $post->ID);
                $time = get_the_time('', $post->ID);
                ?>
                                    <div class="span6">
                                        <div class="row-fluid wide_post_list">
                                            <div class="span12 post_list_thumbnail thumbeffect"><a href='<?php the_permalink(); ?>'>
                                                    <div class="dark-background"><div class="hoverplus"><i class="icon-link"></i></div></div>
                <?php asalah_blog_thumb(375, 136, $post->ID); ?></a></div>
                                            <div class="span12 post_list_title">
                                                <a href='<?php the_permalink(); ?>'><h5><?php the_title(); ?></h5></a>
                                                <div class="post_meta">
                                                    <div class="meta_info">
                                                        <span class="meta_text"><?php echo $date; ?> - <?php echo $time; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        <?php
                    endwhile;
                    ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
        }

        function asalah_post_meta_info() {
            global $post;
            ?>
            <div class="single_post_meta clearfix">
                <span class="meta_element"><i class="icon-time"></i> <?php the_time(get_option('date_format')); ?> - <?php echo get_the_time(); ?></span>

                <?php if (get_the_category_list()): ?>
                    <span class="meta_element"><i class="icon-folder-open"></i> <?php echo get_the_category_list(', '); ?></span>
            <?php endif; ?>

            <?php if (get_the_tag_list()): ?>
                    <span class="meta_element"><?php echo get_the_tag_list('<i class="icon-tags"></i> ', ', '); ?></span>
            <?php endif; ?>

            </div>
            <?php
        }

        function my_new_contactmethods($contactmethods) {
            $contactmethods['twitter'] = __('Twitter', 'asalah');
            $contactmethods['facebook'] = __('Facebook', 'asalah');
            $contactmethods['gplus'] = __('Google Plus', 'asalah');
            $contactmethods['linkedin'] = __('Linkedin', 'asalah');
            return $contactmethods;
        }

        add_filter('user_contactmethods', 'my_new_contactmethods', 10, 1);

        function asalah_bootstrap_pagination($pages = '', $range = 2) {
            $showitems = ($range * 2) + 1;

            global $paged;
            if (empty($paged))
                $paged = 1;
            if ($pages == '') {
                global $wp_query;
                $pages = $wp_query->max_num_pages;
                if (!$pages) {
                    $pages = 1;
                }
            }

            if (1 != $pages) {
                echo "<div class='pagination'><ul>";
                if ($paged > 2 && $paged > $range + 1 && $showitems < $pages)
                    echo "<li><a href='" . get_pagenum_link(1) . "'>&laquo;</a></li>";
                if ($paged > 1 && $showitems < $pages)
                    echo "<li><a href='" . get_pagenum_link($paged - 1) . "'>&lsaquo;</a></li>";

                for ($i = 1; $i <= $pages; $i++) {
                    if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems )) {
                        echo ($paged == $i) ? "<li class='active'><span class='current'>" . $i . "</span></li>" : "<li><a href='" . get_pagenum_link($i) . "' class='inactive' >" . $i . "</a></li>";
                    }
                }

                if ($paged < $pages && $showitems < $pages)
                    echo "<li><a href='" . get_pagenum_link($paged + 1) . "'>&rsaquo;</a></li>";
                if ($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages)
                    echo "<li><a href='" . get_pagenum_link($pages) . "'>&raquo;</a></li>";
                echo "</ul></div>\n";
            }
        }

        function asalah_blog_posts($number = '3', $tag_ids = '') {

            $args = array('post_type' => 'post', 'posts_per_page' => $number);

            if ($tag_ids != '') {
                $tags = explode(',', $tag_ids);
                $tags_array = array();
                if (count($tags) > 0) {
                    foreach ($tags as $tag) {
                        if (!empty($tag)) {
                            $tags_array[] = $tag;
                        }
                    }
                }

                $args['tag_slug__in'] = $tags_array;
            }

            $wp_query = new WP_Query($args);
            ?>
    <?php if ($wp_query) : ?>
                <div class="row-fluid">
                    <div class="span12">
        <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                            <div class="the_blog_post">
                                <div class="post_date_thumbnail">
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php asalah_blog_thumb("520", "337") ?></a>
                                </div>
                                <div class="post_details">
                                    <div class="blog_title"><a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a></div>
                                    <div class="blog_title"><?php echo excerpt(20); ?></div>
                                    <div class="read_more_link"><a href="<?php the_permalink(); ?>"><?php _e("Read more ...", "asalah"); ?></a></div>
                                </div>
                            </div>
                <?php endwhile; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php
        }

        function asalah_posts_carousel($block_id = "", $posttype = 'project', $url = "", $number = '9', $title = "Projects", $desc = "", $max = "4", $cycle = "", $pos = "", $exception = '', $tag_ids = '') {
            ?>

            <?php
            global $post;

            if ($exception == '') {
                $args = array('post_type' => $posttype, 'posts_per_page' => $number);
            } else {
                $args = array('post_type' => $posttype, 'posts_per_page' => $number, 'post__not_in' => array($exception));
            }

            if ($tag_ids != '') {
                $args['tagportfolio'] = $tag_ids;
            }

            $wp_query = new WP_Query($args);
            ?>  
    <?php if ($wp_query) : ?>
                <div class="row-fluid">
                                <?php if ($pos == "side") : ?>
                        <div class="span3">
                            <div class="portfolio_section_title"><h3 class="page-header">
                                    <?php if ($url == ''): ?>
                                        <span class="page_header_title"><?php echo $title; ?></span>
            <?php else: ?>
                                        <a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><span class="page_header_title"><?php echo $title; ?></span></a>
            <?php endif; ?>
                                    <span id="right_car_arrow1" class="cars_arrow_control right_car_arrow"><i class="icon-angle-right"></i></span><span id="left_car_arrow1" class="cars_arrow_control left_car_arrow"><i class="icon-angle-left"></i></span></h3></div>
                            <div class="portfolio_section_title">
                                <p><?php echo $desc; ?></p>
                            </div>
                        </div>

                        <div class="portfolio_carousel span9">
                            <div class="carousel">
                                <div class="slides row-fluid list_carousel responsive clearfix">
                                    <div class="portfolio_cars">
                                                <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                                            <div class="the_portfolio_list_li_div" id="post-<?php the_ID(); ?>">
                                                <div class="portfolio_item">
                <?php asalah_slideshow(); ?>
                                                    <div class="portfolio_thumbnail">
                <?php asalah_blog_thumb("520", "337") ?>
                                                        <div class="portfolio_overlay">
                                                        </div>
                                                        <div class="center-bar">
                                                            <a class="prettyPhotolink icon-search goup" rel="slideshow_<?php echo $post->ID; ?>"></a>
                                                        </div>
                                                    </div>
                                                    <div class="portfolio_info">
                                                        <a href="<?php the_permalink(); ?>"><h5><?php the_title(); ?></h5></a>
                                                        <span class="portfolio_category"><?php $tags_list = get_the_term_list($post->ID, 'tagportfolio', '', ', ', '');
                echo $tags_list; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                        <?php endwhile; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                                        <?php elseif ($pos == "top"): ?>
                        <div class="span12">
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="portfolio_section_title"><h3 class="page-header">
                                            <?php if ($url == ''): ?>
                                                <span class="page_header_title"><?php echo $title; ?></span>
            <?php else: ?>
                                                <a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><span class="page_header_title"><?php echo $title; ?></span></a>
            <?php endif; ?>
                                            <span id="right_car_arrow1" class="cars_arrow_control right_car_arrow"><i class="icon-angle-right"></i></span><span id="left_car_arrow1" class="cars_arrow_control left_car_arrow"><i class="icon-angle-left"></i></span></h3></div>
                                    <div class="portfolio_section_title">
                                        <p><?php echo $desc; ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="row-fluid">
                                <div class="portfolio_carousel span12">
                                    <div class="carousel">
                                        <div class="slides row-fluid list_carousel responsive clearfix">
                                            <div class="portfolio_cars">
                                                            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                                                    <div class="the_portfolio_list_li_div" id="post-<?php the_ID(); ?>">
                                                        <div class="portfolio_item">
                <?php asalah_slideshow(); ?>
                                                            <div class="portfolio_thumbnail">
                <?php asalah_blog_thumb("520", "337") ?>
                                                                <div class="portfolio_overlay">
                                                                </div>
                                                                <div class="center-bar">
                                                                    <a class="prettyPhotolink icon-search goup" rel="slideshow_<?php echo $post->ID; ?>"></a>
                                                                </div>

                                                            </div>
                                                            <div class="portfolio_info">
                                                                <a href="<?php the_permalink(); ?>"><h5><?php the_title(); ?></h5></a>
                                                                <span class="portfolio_category"><?php $tags_list = get_the_term_list($post->ID, 'tagportfolio', '', ', ', '');
                echo $tags_list; ?></span>
                                                            </div>

                                                        </div>
                                                    </div>
            <?php endwhile; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                    <?php elseif ($pos == "hidden"): ?>
                        <div class="portfolio_carousel span12">
                            <div class="carousel">
                                <div class="slides row-fluid list_carousel responsive clearfix">
                                    <ul class="portfolio_cars">
                                                    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                                            <li id="post-<?php the_ID(); ?>">
                                                <div class="portfolio_item">
                <?php asalah_slideshow(); ?>
                                                    <div class="portfolio_thumbnail">
                <?php asalah_blog_thumb("520", "337") ?>
                                                        <div class="portfolio_overlay">
                                                        </div>
                                                        <div class="center-bar">
                                                            <a class="prettyPhotolink icon-search goup" rel="slideshow_<?php echo $post->ID; ?>"></a>
                                                        </div>
                                                    </div>
                                                    <div class="portfolio_info">
                                                        <a href="<?php the_permalink(); ?>"><h5><?php the_title(); ?></h5></a>
                                                        <span class="portfolio_category"><?php $tags_list = get_the_term_list($post->ID, 'tagportfolio', '', ', ', '');
                            echo $tags_list; ?></span>
                                                    </div>
                                                </div>
                                            </li>
            <?php endwhile; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
        <?php endif; ?>
                    <script type="text/javascript" language="javascript">
                        jQuery(window).load(function() {

                        //	Responsive layout, resizing the items
                        jQuery('#<?php echo $block_id; ?> .portfolio_cars').carouFredSel({
                        responsive: true,
                                prev: '#<?php echo $block_id; ?> #left_car_arrow1',
                                next: '#<?php echo $block_id; ?> #right_car_arrow1',
                                auto: false,
        <?php if ($cycle != '') : ?>
                            scroll: {
                            items: <?php echo $cycle; ?>,
                            },
        <?php endif; ?>
                        swipe: {
                        onTouch: true,
                        },
                                items: {
                        visible: {
                        min: 1,
                                max: <?php echo $max; ?>
                        }
                        }
                        });
                        });            </script>
                </div>
            <?php endif; ?>

            <?php
        }

        function asalah_team_carousel($block_id = "", $url = "", $number = '8', $title = "Team Members", $desc = "", $max = "4", $cycle = "") {
            global $post;
            ?>

            <?php
            $wp_query = new WP_Query(array('post_type' => 'team', 'posts_per_page' => $number));
            ?>  
    <?php if ($wp_query) : ?>
                <div id="<?php echo $block_id; ?>" class="row-fluid">

                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="portfolio_section_title"><h3 class="page-header">
                                        <?php if ($url == ''): ?>
                                            <span class="page_header_title"><?php echo $title; ?></span>
                                        <?php else: ?>
                                            <a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a>
        <?php endif; ?>
                                        <span class="right_car_arrow3 cars_arrow_control right_car_arrow"><i class="icon-angle-right"></i></span><span class="left_car_arrow3 cars_arrow_control left_car_arrow"><i class="icon-angle-left"></i></span></h3></div>
                                <div class="portfolio_section_title">
                                    <p><?php echo $desc; ?></p>
                                </div>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="team_carousel span12">
                                <div class="carousel">
                                    <div class="slides row-fluid list_carousel responsive clearfix">
                                        <div class="team_cars">
        <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <?php $get_meta = get_post_custom($post->ID); ?>
                                                <div class="the_portfolio_list_li_div" id="post-<?php the_ID(); ?>">
                                                    <div class="team_item portfolio_item">
                                                        <div class="portfolio_thumbnail">
            <?php asalah_blog_thumb("500", "528") ?>
                                                        </div>
                                                        <div class="portfolio_info">
                                                            <h4><?php the_title(); ?></h4>
                                                            <?php if ($get_meta['asalah_team_position'][0] != ''): ?> 
                                                                <div class="portfolio_time"><?php echo $get_meta['asalah_team_position'][0]; ?></div>
                                                            <?php endif; ?>


                                                                    <?php if ($get_meta['asalah_team_fb'][0] != '' || $get_meta['asalah_team_tw'][0] != '' || $get_meta['asalah_team_gp'][0] != '' || $get_meta['asalah_team_linked'][0] != '' || $get_meta['asalah_team_pin'][0] != '' || $get_meta['asalah_team_mail'][0] != '') { ?>
                                                                <div class="team_social_bar clearfix">
                                                                    <ul class="team_social_list">
                                                                        <?php if ($get_meta['asalah_team_fb'][0] != '') { ?>
                                                                            <li><a href="<?php echo $get_meta['asalah_team_fb'][0]; ?>"><i class="icon-facebook" title="Facebook"></i></a></li>
                                                                        <?php } ?>
                                                                        <?php if ($get_meta['asalah_team_tw'][0] != '') { ?>
                                                                            <li><a href="<?php echo $get_meta['asalah_team_tw'][0]; ?>"><i class="icon-twitter" title="Twitter"></i></a></li>
                                                                        <?php } ?>
                                                                        <?php if ($get_meta['asalah_team_gp'][0] != '') { ?>
                                                                            <li><a href="<?php echo $get_meta['asalah_team_gp'][0]; ?>"><i class="icon-gplus" title="Google Plus"></i></a></li>
                                                                        <?php } ?>
                                                                        <?php if ($get_meta['asalah_team_linked'][0] != '') { ?>
                                                                            <li><a href="<?php echo $get_meta['asalah_team_linked'][0]; ?>"><i class="icon-linkedin" title="Linkedin"></i></a></li>
                                                                        <?php } ?>
                                                                        <?php if ($get_meta['asalah_team_pin'][0] != '') { ?>
                                                                            <li><a href="<?php echo $get_meta['asalah_team_pin'][0]; ?>"><i class="icon-pinterest" title="Pinterest"></i></a></li>
                                                                        <?php } ?>
                                                                        <?php if ($get_meta['asalah_team_mail'][0] != '') { ?>
                                                                            <li><a href="mailto:<?php echo $get_meta['asalah_team_mail'][0]; ?>"><i class="icon-mail" title="Mail"></i></a></li>
                                                                <?php } ?>
                                                                    </ul>
                                                                </div>
            <?php } ?>


                                                        </div>
                                                    </div>
                                                </div>
        <?php endwhile; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript" language="javascript">
                                jQuery(window).load(function() {

                        //	Responsive layout, resizing the items
                        jQuery('#<?php echo $block_id; ?> .team_cars').carouFredSel({
                        responsive: true,
                                prev: '#<?php echo $block_id; ?> .left_car_arrow3',
                                next: '#<?php echo $block_id; ?> .right_car_arrow3',
                                auto: false,
        <?php if ($cycle != '') : ?>
                            scroll: {
                            items: <?php echo $cycle; ?>,
                            },
        <?php endif; ?>
                        swipe: {
                        onTouch: true,
                        },
                                items: {
                        visible: {
                        min: 1,
                                max: <?php echo $max; ?>
                        }
                        }
                        });
                        });            </script>
                </div>
            <?php endif; ?>

            <?php
        }

        function asalah_testimonials_carousel($block_id = "", $number = '9', $title = "Testimonials") {
            ?>

            <?php
            $wp_query = new WP_Query(array('post_type' => 'testimonial', 'posts_per_page' => $number));
            ?>  
            <?php if ($wp_query) : ?>
                <h3 class="page-header"><span class="page_header_title"><?php echo $title; ?></span><span id="right_car_arrow2" class="cars_arrow_control right_car_arrow"><i class="icon-angle-right"></i></span><span id="left_car_arrow2" class="cars_arrow_control left_car_arrow"><i class="icon-angle-left"></i></span></h3>
                <div class="testimonial_content carousel list_carousel responsive clearfix">
                    <ul class="testy_carousel clearfix">
                        <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                            <li class="clearfix testimonials_area">
                                <?php
                                if (has_post_thumbnail(get_the_ID())) {
                                    $image_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                                    ?>
                                    <div class="span4 testimonials_image">


                                        <?php the_post_thumbnail(''); ?>

                                    </div>
                                <?php } ?>
                                <div class="span8 testimonials_info">
                                    <div class="testimonial_box ">
                                        <p><?php the_content(); ?></p>
                                    </div>
                                    <a class="testimonial_url" target="_blank" href="<?php testimonial_url(); ?>">
                                        <div class="tetimonials_namejob clearfix">
                                            <span class="testimonial_name"><?php testimonial_author(); ?></span><span class="testimonial_job"><?php testimonial_job(); ?></span>
                                        </div>
                                    </a>
                                </div>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                </div>
                <script type="text/javascript" language="javascript">
                            jQuery(window).load(function() {

                    //	Responsive layout, resizing the items
                    jQuery('.testy_carousel').carouFredSel({
                    responsive: true,
                            prev: '#left_car_arrow2',
                            next: '#right_car_arrow2',
                            auto: false,
                            scroll: {
                    fx: "cover-fade",
                    },
                            swipe: {
                    onTouch: true,
                    },
                            items: {
                    visible: {
                    min: 1,
                            max: 1
                    }
                    }
                    });
                    });        </script>
    <?php endif; ?>

            <?php
        }

        function asalah_clients_carousel($block_id = "", $number = '9', $title = 'Clients') {
            global $post;
            ?>

            <?php
            $wp_query = new WP_Query(array('post_type' => 'Client', 'posts_per_page' => $number));
            ?>  
            <?php if ($wp_query) : ?>
                <h3 class="page-header"><span class="page_header_title"><?php echo $title; ?></span><span id="right_car_arrow3" class="cars_arrow_control right_car_arrow"><i class="icon-angle-right"></i></span><span id="left_car_arrow3" class="cars_arrow_control left_car_arrow"><i class="icon-angle-left"></i></span></h3>
                <div class="clients_content">
                    <div class="clients_box ">
                        <ul class="clients_list">
        <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

            <?php $the_client_url = get_post_meta($post->ID, 'client_url', true); ?>

                                <li><a style="display:block" href="<?php echo $the_client_url; ?>" class="post-tooltip tooltip-n" original-title="<?php the_title(); ?>" target="_blank"><div class="client_item clearfix" style="position: relative;"><?php client_logo(); ?></div></a></li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
                <script type="text/javascript" language="javascript">
                            jQuery(window).load(function() {

                    //	Responsive layout, resizing the items
                    jQuery('.clients_list').carouFredSel({
                    responsive: true,
                            prev: '#left_car_arrow3',
                            next: '#right_car_arrow3',
                            auto: false,
                            height: "100%",
                            scroll: {
                    items: 1,
                    },
                            swipe: {
                    onTouch: true,
                    },
                            items: {
                    width: 220,
                            visible: {
                    min: 1,
                            max: 6
                    }
                    }
                    });
                    });
                </script>
    <?php endif; ?>
    <?php
}

function asalah_posts_list($posttype = 'post', $number = '3', $order = "date") {
    ?>

            <?php
            $wp_query = new WP_Query(array('post_type' => $posttype, 'posts_per_page' => $number, 'orderby' => $order));
            ?>  
            <?php if ($wp_query) : ?>

                <div class="post_list itswidget">
                <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                        <div class="post_row clearfix">
                            <div class="post_thumbnail"><a class="thumbnail" href="<?php the_permalink(); ?>"><?php asalah_blog_thumb("370", "240", "", "thumbnail") ?></a></div>
                            <div class="post_info">
                                <div class="post_title"><a href="<?php the_permalink(); ?>"><h6><?php the_title(); ?></h6></a></div>
                                <span class="blog_date meta_item"><?php echo get_the_date(); ?></span>

                            </div>
                        </div>
        <?php endwhile; ?>
                </div>
    <?php endif; ?>

    <?php
}

function asalah_breadcrumbs($last = "") {
    global $asalah_data;
    if (!is_home() && !$asalah_data['asalah_disable_breadcrumb']) {
        echo '<nav class="breadcrumb">';
        echo '<a href="' . home_url('/') . '">' . __("Home", "asalah") . '</a> <span class="divider">&raquo;</span> ';
        if (is_category()) {
            the_category(' <span class="divider">&raquo;</span> ');
        } elseif (is_single()) {
            if (get_post_type() != 'post') {
                $post_type = get_post_type_object(get_post_type());
                if (get_post_type() == 'post') {
                    if ($asalah_data['asalah_blog_url']) {
                        echo '<a href="' . $asalah_data['asalah_blog_url'] . '">';
                    }
                    echo $post_type->labels->name;
                    if ($asalah_data['asalah_blog_url']) {
                        echo '</a>';
                    }
                } elseif (get_post_type() == 'project') {
                    if ($asalah_data['asalah_portfolio_url']) {
                        echo '<a href="' . $asalah_data['asalah_portfolio_url'] . '">';
                    }
                    echo $post_type->labels->name;
                    if ($asalah_data['asalah_portfolio_url']) {
                        echo '</a>';
                    }
                } else {
                    echo $post_type->labels->name;
                }

                echo ' <span class="divider">&raquo;</span> ';
                the_title();
            } else {
                the_category(' <span class="divider">&raquo;</span> ');
                echo ' <span class="divider">&raquo;</span> ';
                the_title();
            }
        } elseif (is_page()) {
            echo the_title();
        }
        if ($last != "") {
            echo " " . $last;
        }
        echo '</nav>';
    }
}

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    return $excerpt;
}

function random_id($length) {
    $characters = '23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ';
    $max = strlen($characters) - 1;
    $string = '';

    for ($i = 0; $i < $length; $i++) {
        $string .= $characters[mt_rand(0, $max)];
    }

    return $string;
}

function get_twitter_followers($url) {
    $data = file_get_contents("http://query.yahooapis.com/v1/public/yql?q=SELECT%20*%20from%20html%20where%20url=%22" . $url . "%22%20AND%20xpath=%22//a[@class='js-nav']/strong%22&format=json"); // Opening the Query URL
    $data = json_decode($data); // Decoding the obtained JSON data
    $count = intval($data->query->results->strong[2]); // The count parsed from the JSON
    return $count; // Printing the count
}

function asalah_translat($id, $word) {
    global $asalah_data;
    if ($asalah_data[$id]) {
        echo $asalah_data[$id];
    } else {
        _e($word, 'asalah');
    }
}

function asalah_twitter_tweets($consumerkey = '', $consumersecret = '', $accesstoken = '', $accesstokensecret = '', $screenname = '', $tweets_count = 2) {
    
    if (empty($consumerkey) || empty($consumersecret) || empty($accesstokensecret) || empty($accesstoken)) {
        return 'Your twitter application info is not set correctly in option panel, please create please login to twitter developers <a href="https://dev.twitter.com/apps" target="_blank">here</a>, create new application and new access tocken, then go to theme option panel social section and fill the data you got from application';
    }else{
       $twitter = new TwitterOAuth($consumerkey, $consumersecret, $accesstoken, $accesstokensecret);

    $tweets = $twitter->get('statuses/user_timeline', array('screen_name' => $screenname, 'count' => $tweets_count));

    $output = '';

    if (is_array($tweets) && !isset($tweets->errors)) {
        $i = 0;
        $lnk_msg = NULL;

        $output .= "<ul>";
        foreach ($tweets as $tweet) {
            $i++;

            $lnk_page = 'http://twitter.com/#!/' . $screenname;
            $page_name = $tweet->user->name;

            $msg = $tweet->text;

            if (is_array($tweet->entities->urls)) {
                try {
                    if (array_key_exists('0', $tweet->entities->urls)) {
                        $lnk_msg = $tweet->entities->urls[0]->url;
                    } else {
                        $lnk_msg = NULL;
                    }
                } catch (Exception $e) {
                    $lnk_msg = NULL;
                }
            }



            $lnk_tweet = 'http://twitter.com/#!/' . $screenname . '/status/' . $tweet->id_str;


            /* Tweet Time */
            $time = strtotime($tweet->created_at);
            $delta = abs(time() - $time); /* in seconds */
            $result = '';
            if ($delta < 1) {
                $result = ' just now';
            } elseif ($delta < 60) {
                $result = $delta . ' seconds ago';
            } elseif ($delta < 120) {
                $result = ' about a minute ago';
            } elseif ($delta < (45 * 60)) {
                $result = ' about ' . round(($delta / 60), 0) . ' minutes ago';
            } elseif ($delta < (2 * 60 * 60)) {
                $result = ' about an hour ago';
            } elseif ($delta < (24 * 60 * 60)) {
                $result = ' about ' . round(($delta / 3600), 0) . ' hours ago';
            } elseif ($delta < (48 * 60 * 60)) {
                $result = ' about a day ago';
            } else {
                $result = ' about ' . round(($delta / 86400), 0) . ' days ago';
            }


            if ($i >= $tweets_count)
                break;


            $output .= '<li class="cat-item"><a href="'.$lnk_tweet.'" class="tweet_icon"><i class="icon-twitter"></i></a> <a class="tweet_name" href="'.$lnk_tweet.'">' . $screenname . '</a>';
            
            if (!is_null($lnk_msg)) {
                $output .= sprintf('<a href="%1$s">%2$s</a>', $lnk_msg, $msg);
                //$output .= '<a href="' . $lnk_msg . '">' . $msg . '</a>';
            } else {
                $output .= $msg;
            }
            
            $output .= '<span class="tweet_time">' . $result . '</span></li>';
            
        } /* foreach */

        $output .= "</ul>";
        return $output;
        if (!empty($output)) {
            //return; $output;
        }
    } else {
        if (isset($tweets->errors)):
            $output .= '<span class="tweet_error">Message: ' . $tweets->errors[0]->message . ', Please check your Twitter Authentication Data or internet connection.</span>';
        else:
            $output .= '<span class="tweet_error">Please check your internet connection.</span>';
        endif;

        if (!empty($output)) {
            return $output;
        }
    } 
    }
    
}

?>