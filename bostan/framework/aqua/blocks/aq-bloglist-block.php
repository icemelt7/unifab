<?php
/** Notifications block **/

if(!class_exists('AQ_bloglist_Block')) {
	class AQ_bloglist_Block extends AQ_Block {
		
		//set and create block
		function __construct() {
			$block_options = array(
				'name' => 'Blog Posts List',
				'size' => 'span6',
			);
			
			//create the block
			parent::__construct('AQ_bloglist_Block', $block_options);
		}
		
		function form($instance) {
			
			$defaults = array(
				'title' => '',
				'url' => '',
				'postnumber' => '',
				'tags_ids' => ''
			);
			$instance = wp_parse_args($instance, $defaults);
			extract($instance);
			
			?>
			
			<p class="description">
				<label for="<?php echo $this->get_field_id('title') ?>">
					Title<br/>
					<?php echo aq_field_input('title', $block_id, $title) ?>
				</label>
			</p>
            <p class="description">
				<label for="<?php echo $this->get_field_id('url') ?>">
					Blog Page URL<br/>
					<?php echo aq_field_input('url', $block_id, $url) ?>
				</label>
			</p>
			<p class="description">
				<label for="<?php echo $this->get_field_id('postnumber') ?>">
					Number Of Posts<br/>
					<?php echo aq_field_input('postnumber', $block_id, $postnumber) ?>
				</label>
			</p>
            <p class="description">
				<label for="<?php echo $this->get_field_id('tags_ids') ?>">
					Tags (Seperated by comma)<br/>
					<?php echo aq_field_input('tags_ids', $block_id, $tags_ids) ?>
				</label>
			</p>
			<?php
			
		}
		
		function block($instance) {
			extract($instance);
			$the_id = "aq-block-" . $number;
			?>
			<div class="span12">
			<?php if ($title) : ?>
                <h3 class="page-header">
                	<?php if ($url) : ?>
                    	<a href="<?php echo $url; ?>"><span class="page_header_title"><?php echo strip_tags($title); ?></span></a>
                    <?php else: ?>
                    	<span class="page_header_title"><?php echo strip_tags($title); ?></span>
                    <?php endif; ?>
                </h3>
			<?php endif; ?>
            
			<?php asalah_blog_posts($postnumber, $tags_ids); ?>
			</div>
			<?php
		}
		
	}
}