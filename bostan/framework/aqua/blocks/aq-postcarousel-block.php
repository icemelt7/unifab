<?php
/** Notifications block **/

if(!class_exists('AQ_Postcars_Block')) {
	class AQ_Postcars_Block extends AQ_Block {
		
		//set and create block
		function __construct() {
			$block_options = array(
				'name' => 'Projects Carousel',
				'size' => 'span12',
			);
			
			//create the block
			parent::__construct('AQ_Postcars_Block', $block_options);
		}
		
		function form($instance) {
			
			$defaults = array(
				'title' => '',
				'description' => '',
				'url' => '',
				'postnumber' => '',
				'max' => '',
				'cycle' => '',
				'pos' => '',
				'thewidth' => '',
				'tags_ids' => ''
			);
			$instance = wp_parse_args($instance, $defaults);
			extract($instance);
			$post_types = array(
				'project' => 'Projects',
				'post' => 'Posts'
			);
			$positions = array(
				'top' => 'Top',
				'side' => 'Left Side',
				'hidden' => 'Hidden',
			);
			$widthes = array(
				'container' => 'Container',
				'fluid' => 'Fluid',
			);
			?>
			
            <p class="description the_width_field">
				<label for="<?php echo $this->get_field_id('thewidth') ?>">
					Width<br/>
					<?php echo aq_field_select('thewidth', $block_id, $widthes, $thewidth) ?>
				</label>
			</p>
            
			<p class="description">
				<label for="<?php echo $this->get_field_id('title') ?>">
					Title<br/>
					<?php echo aq_field_input('title', $block_id, $title) ?>
				</label>
			</p>
			<p class="description">
				<label for="<?php echo $this->get_field_id('desc') ?>">
					Description Text<br/>
					<?php echo aq_field_textarea('desc', $block_id, $desc, $size = 'full') ?>
				</label>
			</p>
			<p class="description">
				<label for="<?php echo $this->get_field_id('url') ?>">
					Portfolio Page URL<br/>
					<?php echo aq_field_input('url', $block_id, $url) ?>
				</label>
			</p>
			<p class="description">
				<label for="<?php echo $this->get_field_id('postnumber') ?>">
					Number Of Posts<br/>
					<?php echo aq_field_input('postnumber', $block_id, $postnumber) ?>
				</label>
			</p>
			<p class="description">
				<label for="<?php echo $this->get_field_id('max') ?>">
					Max Number To Appear In Page<br/>
					<?php echo aq_field_input('max', $block_id, $max) ?>
				</label>
			</p>
			<p class="description">
				<label for="<?php echo $this->get_field_id('cycle') ?>">
					Number Of Items To Switch Each Cycle<br/>
					<?php echo aq_field_input('cycle', $block_id, $cycle) ?>
				</label>
			</p>
			<p class="description">
				<label for="<?php echo $this->get_field_id('pos') ?>">
					Heading Position<br/>
					<?php echo aq_field_select('pos', $block_id, $positions, $pos) ?>
				</label>
			</p>
            <p class="description">
				<label for="<?php echo $this->get_field_id('tags_ids') ?>">
					Tags (Seperated by comma)<br/>
					<?php echo aq_field_input('tags_ids', $block_id, $tags_ids) ?>
				</label>
			</p>
			<?php
			
		}
		
		function block($instance) {
			extract($instance);
			$the_id = "aq-block-" . $number;
			?>
			<div class="row-fluid">
			<div class="span12">
			<?php asalah_posts_carousel($the_id, 'project', $url, $postnumber, $title, $desc, $max, $cycle, $pos, '', $tags_ids); ?>
			</div>
			</div>
			
			<?php
		}
		
	}
}