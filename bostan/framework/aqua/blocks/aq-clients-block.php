<?php
/** Notifications block **/

if(!class_exists('AQ_clients_Block')) {
	class AQ_clients_Block extends AQ_Block {
		
		//set and create block
		function __construct() {
			$block_options = array(
				'name' => 'Clients Carousel',
				'size' => 'span12',
			);
			
			//create the block
			parent::__construct('AQ_clients_Block', $block_options);
		}
		
		function form($instance) {
			
			$defaults = array(
				'title' => '',
				'postnumber' => '',
				'thewidth' => ''
			);
			$instance = wp_parse_args($instance, $defaults);
			extract($instance);
			$widthes = array(
				'container' => 'Container',
				'fluid' => 'Fluid',
			);
			?>
			<p class="description the_width_field">
				<label for="<?php echo $this->get_field_id('thewidth') ?>">
					Width<br/>
					<?php echo aq_field_select('thewidth', $block_id, $widthes, $thewidth) ?>
				</label>
			</p>
			<p class="description">
				<label for="<?php echo $this->get_field_id('title') ?>">
					Title (optional)<br/>
					<?php echo aq_field_input('title', $block_id, $title) ?>
				</label>
			</p>
			<p class="description">
				<label for="<?php echo $this->get_field_id('postnumber') ?>">
					Number<br/>
					<?php echo aq_field_input('postnumber', $block_id, $postnumber) ?>
				</label>
			</p>
			<?php
			
		}
		
		function block($instance) {
			extract($instance);
			$the_id = "aq-block-" . $number;
			?>
			<div class="span12">
			<?php asalah_clients_carousel($the_id,$postnumber, $title); ?>
			</div>
			
			<?php
		}
		
	}
}